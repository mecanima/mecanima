# Mecanima

### https://mecanima.fr

## Getting Started

First, run the development server:

```bash
yarn dev
# or
npm run dev
```

> Note: you may need HTTPS to use deviceOrientation events (needed for the compass and the telescope). You can run the server in HTTPS mode by runnning `yarn dev-ssl`. It'll use certificate and private key in this order from your home folder using these names: `openssl.crt` and `openssl.key`. You can also pass `$CERT` and `$PRIV_KEY` env variables by removing new lines in strings.

You can then checkout the result in your browser at http://localhost:3000/