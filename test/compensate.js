const fromAbsoluteToRange = (val, min) => {
    let missing = val / (val - min);
    let ahem = val + missing * Math.abs(min);
    return ahem;
};

console.log(fromAbsoluteToRange(-280, -180));
console.log(fromAbsoluteToRange(-100, -180));
console.log(fromAbsoluteToRange(100, -180));
console.log(fromAbsoluteToRange(280, -180));
