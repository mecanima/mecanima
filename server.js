const path = require("path");
const homeDir = require("os").homedir();
const dev = process.env.NODE_ENV !== "production";
const ssl = process.env.SSL === "true";

const { createServer } = !dev || ssl ? require("https") : require("http");
const { parse } = require("url");
const next = require("next");
const socketIO = require("socket.io");
const socketManagement = require("./srv/global");
const fs = require("fs");

const app = next({ dev });
const handle = app.getRequestHandler();

if (!dev) {
    const Sentry = require("@sentry/node");
    const SentryIntegrations = require("@sentry/integrations");
    Sentry.init({
        dsn: process.env.SENTRY_DSN,
        release: "mecanima@latest",
        integrations: [
            new SentryIntegrations.CaptureConsole({
                levels: ["error"],
            }),
            new SentryIntegrations.Dedupe(),
            new SentryIntegrations.ExtraErrorData(),
            new SentryIntegrations.Transaction(),
        ],
        maxBreadcrumbs: 50,
    });
}

app.prepare().then(() => {
    const requestListener = (req, res) => {
        const parsedUrl = parse(req.url, true);
        handle(req, res, parsedUrl);
    };
    const server =
        !dev || ssl
            ? createServer(
                  {
                      cert: process.env.CERT
                          ? JSON.parse(`"${process.env.CERT}"`)
                          : fs.readFileSync(path.join(homeDir, "openssl.crt")),
                      key: process.env.PRIV_KEY
                          ? JSON.parse(`"${process.env.PRIV_KEY}"`)
                          : fs.readFileSync(path.join(homeDir, "openssl.key")),
                  },
                  requestListener
              )
            : createServer(requestListener);
    const io = socketIO(server);
    socketManagement(io);
    server.listen(3000, (err) => {
        if (err) throw err;
        console.log("> Ready on http://localhost:3000");
    });
});
