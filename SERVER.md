# Socket API

```
Event: "setup"
Arguments:
- id: A UUID for the user (needs to be persisted to allow reconnection)
```

```
Event: "room"
Arguments:
- action: A string representing a "function". Possible values: join,quit,start
- payload: An object representing needed values for actions
```
### Outgoing actions
Arguments for actions (`data.roomId`, etc.):

-   join:
    -   `roomId`: the room ID allowing the player to join the room . It needs to starts with a letter that indicates what game it is. Currently we only use the letter O for "Outils".

    Returns room's data
-   quit:
    -   `roomId`: the room ID allowing the player to quit the room
-   start: (no arguments currently)

    Returns room's data  
<br>
---

```
Event: "game"
Arguments:
- action: A string representing a "function". Possible values: goToStep,entryAnswer,finalAnswer
- payload: An object representing needed values for actions
```

### Outgoing actions

Arguments for actions (`data.step`, etc.):

-   goToStep:
    -   `step`: the ID of the mission.

    Returns nothing, but sends the data to everyone with the "step" action and the game data in the payload
-   entryAnswer:
    -   `text`: the text the player typed

    Returns an object with the text and the result (true if that was the answer)
-   finalAnswer:
    -   `text`: idem for the final answer of the mission

    Same as before
-   tool:
    -   `tool`: the name of the tool that is being picked up or released
    -   `side`: true if the tool is being picked up or false if the user is releasing it
    Returns true if the tool has been successfully picked up, false otherwise 

### Incomming actions 
-   step:
    Returns all game data
-   entryAnswer:
    Returns same data as outgoing
-   finalAnswer:
    Returns same data as outgoing
-   tool:
    Returns all the tool's state array

<br>

Models:

-   User:
    -   `id` The UUID sent by the client
    -   `character` The associated character (will change according to game)
    -   `room` The id of the room
    -   ...?
-   Room:
    -   `id`: The ID of the room (determined by the code sent by the first player, IE: you can currently create any room with any ID)
    -   `users`: Array of users currently in the room (doesn't show disconnected)
    -   `state`: Indicate the state of the room: started
-   Game:
    -   `currentStep`: The data of the "mission" currently choosen
        -   `entryAnswer`: An object containing the `text` already typed and its `length`
        -   `finalAnswer`: Same
        -   `tools`: An array of the tools in the step
    -   `stepsFinalAnswers`: An array containing all the finalAnswers for the missions


<br>

Room states:
    - started (whether you're waiting for players or not)
Game states:
    - starting-screen (explanation text)
    - is-mission-menu (selecting mission)
    - is-mission1-intro