const withSourceMaps = require("@zeit/next-source-maps")();
const withVideos = require("next-videos");
const optimizedImages = require("next-optimized-images");
const { PHASE_DEVELOPMENT_SERVER } = require("next/constants");
const path = require("path");

const withFonts = require("next-fonts");
module.exports = withFonts();

module.exports = (phase) =>
    withVideos(
        optimizedImages(
            withSourceMaps({
                compress: false,
                env: {
                    SENTRY_DSN: process.env.SENTRY_DSN,
                    ENV: process.env.NODE_ENV,
                    RELEASE: "mecanima@latest",
                },
                webpack: (config, { isServer, webpack, buildId }) => {
                    if (isServer) {
                        // Implementation detail of next.js, externals is an array of one function if isServer is true
                        // This avoid a error when building the app to production ¯\_(ツ)_/¯
                        const [externals] = config.externals;
                        config.externals = (context, request, callback) => {
                            if (path.isAbsolute(request)) {
                                return callback();
                            }
                            return externals(context, request, callback);
                        };
                    }
                    // this is from the with-sentry-simple example.
                    if (!isServer) {
                        config.resolve.alias["@sentry/node"] =
                            "@sentry/browser";
                    }
                    // Sentry webpack plugin
                    // Will upload sourcemaps on build
                    if (phase !== PHASE_DEVELOPMENT_SERVER) {
                        const SentryWebpackPlugin = require("@sentry/webpack-plugin");
                        config.plugins.push(
                            new SentryWebpackPlugin({
                                release: "mecanima@latest",
                                include: ".next",
                                urlPrefix: "~/_next/",
                            })
                        );
                    }
                    return config;
                },
            })
        )
    );
