import styles from "./home.module.scss";
import Container from "../../comps/container/container";
import Button from "../../comps/button";
import { useRouter } from "next/router";
import React, { useState } from "react";
import localforage from "localforage";

import RoomStore from "../../store/room";

const splashVideo = require("./splash.mp4");

const Home = () => {
    // const contextValue = useContext(userContext);
    const [room, setRoom] = useState("O" + Math.round(Math.random() * 1000));
    const [videoFinished, setVideoFinished] = useState(false);
    const router = useRouter();
    const setRoomFromInput = (evt) => {
        setRoom(evt.currentTarget.value);
    };

    const submit = () => {
        RoomStore.join(room).then(() => {
            localforage.setItem("room", room);
            router.replace("/room");
        });
    };

    const moveToTop = () => {
        setVideoFinished(true);
    };
    return (
        <div>
            <video
                className={`${styles.splashVideo} ${
                    videoFinished ? styles.finished : ""
                }`}
                src={splashVideo}
                playsInline
                muted
                autoPlay
                onEnded={moveToTop}
                controlsList="disablePictureInPicture"
            ></video>
            <Container
                className={`${styles.container} ${
                    !videoFinished ? styles.invisible : ""
                }`}
            >
                {/* <img className={styles.logo} src={require("./logo.png")} /> */}
                <div className={styles.globalContainer}>
                    <p className={styles.mainText}>
                        Entrez le code de votre équipe
                    </p>
                    <div className={styles.inputContainer}>
                        <img src={require("./input.png")} />
                        <input
                            className={styles.input}
                            type="text"
                            onChange={setRoomFromInput}
                            value={room}
                        ></input>
                    </div>
                </div>
                {/* <Link href="/room"> */}
                {/* <button onClick={submit} className={styles.button_home}><img src={require('./button.png')} /><img className={styles.background} src={require('./fond-944.png')} /><span>Commencer</span></button> */}
                <Button text="Commencer" onClick={submit} abs={true}></Button>
                {/* </Link> */}
            </Container>
        </div>
    );
};

export default Home;
