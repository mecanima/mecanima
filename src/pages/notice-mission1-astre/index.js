import React, { useEffect, useRef } from "react";
import styles from "./notice-mission1-astre.module.scss";
import NoticeHeader from "../../comps/notice-header";
import OtherChapter from "../../comps/other-chapter";
import Header from "../../comps/header";
import ArrowBtn from "../../comps/arrow-btn";
import GlobalWrapper from "../../comps/globalWrapper";
import { useRouter } from "next/router";
import { useIntersection } from "react-use";
import VideoBackground from "../../comps/videoBackground";

import { otherChapters } from "../../utils/otherChapters";

export const resources = {
    videos: { astre2: require("./imgs/astre2.mp4") },
    images: {
        livre: {
            png: require("./imgs/livre.png"),
            webp: require("./imgs/livre.png?webp"),
        },
        astre: {
            png: require("./imgs/astre.png"),
            webp: require("./imgs/astre.png?webp"),
        },
    },
};
const NoticeMission1Astre = () => {
    const router = useRouter();

    let isSecondAppear = false;
    let isThirdAppear = false;
    let isChapterAppear = false;
    const secondText = useRef(null);
    const thirdText = useRef(null);
    const Chapter = useRef(null);
    const video = useRef(null);
    const sectionVideo = useRef(null);

    const secondIntersection = useIntersection(secondText, {
        root: null,
        rootMargin: "0px",
        threshold: 1,
    });
    const thirdIntersection = useIntersection(thirdText, {
        root: null,
        rootMargin: "0px",
        threshold: 1,
    });
    const chapterIntersection = useIntersection(Chapter, {
        root: null,
        rootMargin: "0px",
        threshold: 0.5,
    });
    const videoIntersection = useIntersection(sectionVideo, {
        root: null,
        rootMargin: "0px",
        threshold: 1,
    });

    if (video.current !== null)
        videoIntersection && videoIntersection.intersectionRatio < 1
            ? video.current.pause()
            : video.current.play();
    secondIntersection && secondIntersection.intersectionRatio < 1
        ? (isSecondAppear = true)
        : null;
    thirdIntersection && thirdIntersection.intersectionRatio < 1
        ? (isThirdAppear = true)
        : null;
    chapterIntersection && chapterIntersection.intersectionRatio < 0.5
        ? null
        : (isChapterAppear = true);

    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    return (
        <GlobalWrapper beige>
            <div className={styles.noticeContainer}>
                <NoticeHeader tool="Les astres" main={false}></NoticeHeader>
                <div className={`${styles.sectionContainer} ${styles.mt}`}>
                    {/* Unuseful video, a basic CSS animation can do the same 
                    <VideoBackground
                        loop={false}
                        playsInline
                        autoPlay
                        url={require("./imgs/astre1.mp4")}
                        className={styles.oculaire}
                    ></VideoBackground> */}
                    <picture>
                        <source
                            srcSet={resources.images.astre.webp}
                            type="image/webp"
                        />
                        <source
                            srcSet={resources.images.astre.png}
                            type="image/png"
                        />
                        <img
                            className={styles.astre}
                            src={resources.images.astre.png}
                        />
                    </picture>
                    <div className={styles.textContainer}>
                        <p className={styles.firstAnimText}>
                            La surface de la Lune est plate
                            <span className={styles.fontW2}>
                                {" "}
                                selon les têtes pensantes
                            </span>
                            , mais{" "}
                            <span className={styles.fontW1}>Galilée</span> n’est
                            pas de cet avis. Il lui faudra se battre pour
                            défendre son idée.
                        </p>
                    </div>
                </div>
                <div className={styles.sectionContainer} ref={sectionVideo}>
                    <VideoBackground
                        loop={false}
                        playsInline
                        autoPlay={false}
                        url={resources.videos.astre2}
                        ref={video}
                        className={styles.oculaire}
                    ></VideoBackground>
                    <div className={styles.textContainer}>
                        <p
                            ref={secondText}
                            className={isSecondAppear ? "" : styles.animText}
                        >
                            Ce qui frappe Galilée avant tout, c’est{" "}
                            <span className={styles.oneLine}>
                                <span className={styles.fontW2}>
                                    l’irrégularité
                                </span>{" "}
                                du{" "}
                                <span className={styles.fontW2}>
                                    terminateur
                                </span>
                            </span>
                            . On appelle ainsi la ligne qui sépare la{" "}
                            <span className={styles.fontW2}>nuit</span> du{" "}
                            <span className={styles.fontW2}>jour</span> sur la
                            Lune.
                        </p>
                    </div>
                </div>
                <div className={styles.sectionContainer}>
                    <picture>
                        <source
                            srcSet={resources.images.livre.webp}
                            type="image/webp"
                        />
                        <source
                            srcSet={resources.images.livre.png}
                            type="image/png"
                        />
                        <img
                            className={`${styles.book} ${
                                isThirdAppear ? "" : styles.bookAnim
                            }`}
                            src={resources.images.livre.png}
                        />
                    </picture>
                    <div className={styles.textContainer}>
                        <p
                            ref={thirdText}
                            className={isThirdAppear ? "" : styles.animText}
                        >
                            C’est grâce à ces observations qu’il écrira dans son
                            livre{" "}
                            <span className={styles.italic}>
                                Sidereus Nuncius
                            </span>{" "}
                            que la Lune est “constituée de cavités et de
                            protubérances” avec “les{" "}
                            <span className={styles.fontW2}>
                                crêtes des montagnes
                            </span>{" "}
                            et les profondeurs des{" "}
                            <span className={styles.fontW2}>vallées</span>.”
                        </p>
                    </div>
                </div>
                <div ref={Chapter}>
                    <OtherChapter
                        chapter1={otherChapters.objectif}
                        chapter2={otherChapters.oculaire}
                        appear={isChapterAppear}
                    ></OtherChapter>
                </div>
                <Header left>
                    <ArrowBtn
                        onClick={() => {
                            router.replace("/notice-mission1-menu");
                        }}
                    ></ArrowBtn>
                </Header>
            </div>
        </GlobalWrapper>
    );
};

export default NoticeMission1Astre;
