import React, { useEffect, useRef } from "react";
import styles from "./notice-end-money.module.scss";
import NoticeHeader from "../../comps/notice-header";
import OtherChapter from "../../comps/other-chapter";
import Header from "../../comps/header";
import { useRouter } from "next/router";
import ArrowBtn from "../../comps/arrow-btn";
import VideoBackground from "../../comps/videoBackground";
import GlobalWrapper from "../../comps/globalWrapper";
import { useIntersection } from "react-use";

import { otherChapters } from "../../utils/otherChapters";

export const resources = {
    videos: {
        money1: require("./imgs/money.mp4"),
    },
    images: {},
};

const NoticeEndMoney = () => {
    const router = useRouter();

    let isChapterAppear = false;
    const Chapter = useRef(null);
    const video = useRef(null);
    const sectionVideo = useRef(null);

    const videoIntersection = useIntersection(sectionVideo, {
        root: null,
        rootMargin: "0px",
        threshold: 1,
    });
    const chapterIntersection = useIntersection(Chapter, {
        root: null,
        rootMargin: "0px",
        threshold: 0.5,
    });

    if (video.current !== null)
        videoIntersection && videoIntersection.intersectionRatio < 1
            ? video.current.pause()
            : video.current.play();
    chapterIntersection && chapterIntersection.intersectionRatio < 0.5
        ? null
        : (isChapterAppear = true);

    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    return (
        <GlobalWrapper beige>
            <div className={styles.noticeContainer}>
                <NoticeHeader tool="Le système monétaire" main={false}></NoticeHeader>
                <div className={`${styles.sectionContainer} ${styles.mt}`}>
                    <VideoBackground
                        loop={false}
                        playsInline
                        autoPlay
                        url={resources.videos.money1}
                        className={styles.oculaire}
                    ></VideoBackground>
                    <div className={styles.textContainer}>
                        <p className={styles.firstAnimText}>                       
                            Blaise Pascal a crée la pascaline à 19 ans pour <span className={styles.fontW1}>faciliter</span> la tâche de son père,
                            <span className={styles.fontW1}>commissaire de l’impôt</span>.
                        </p>
                    </div>
                </div>
                <div ref={Chapter}>
                    <OtherChapter
                        chapter1={otherChapters.money}
                        chapter2={otherChapters.machine}
                        appear={isChapterAppear}
                    ></OtherChapter>
                </div>
                <Header left>
                    <ArrowBtn
                        onClick={() => {
                            router.replace("/notice-end-menu");
                        }}
                    ></ArrowBtn>
                </Header>
            </div>
        </GlobalWrapper>
    );
};

export default NoticeEndMoney;
