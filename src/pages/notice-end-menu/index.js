import React from "react";
import { useRouter } from "next/router";
import styles from "./notice-end-menu.module.scss";
import Header from "comps/header";
import BackButton from "comps/back-button";
import NoticeHeader from "comps/notice-header";
import GlobalWrapper from "comps/globalWrapper";
import { otherChapters } from "utils/otherChapters";

export const resources = {
    videos: {},
    images: {
        pascaline: {
            png: require("./imgs/pascaline.png"),
            webp: require("./imgs/pascaline.png?webp"),
        },
        feuille: {
            png: require("./imgs/feuille-de-compte.png"),
            webp: require("./imgs/feuille-de-compte.png?webp"),
        },
    },
};

const NoticeMission1Menu = () => {
    const router = useRouter();
    return (
        <GlobalWrapper beige>
            <NoticeHeader
                tool="de la pascaline"
                main={true}
            ></NoticeHeader>
            <div className={styles.noticeContainer}>
                {/* <img  className={styles.imgTest} /> */}
                <picture>
                    <source
                        srcSet={resources.images.pascaline.webp}
                        type="image/webp"
                    />
                    <source
                        srcSet={resources.images.pascaline.png}
                        type="image/png"
                    />
                    <img
                        className={styles.img}
                        src={resources.images.pascaline.png}
                    />
                </picture>
                <picture>
                    <source
                        srcSet={resources.images.feuille.webp}
                        type="image/webp"
                    />
                    <source
                        srcSet={resources.images.feuille.png}
                        type="image/png"
                    />
                    <img
                        className={styles.feuille}
                        src={resources.images.feuille.png}
                    />
                </picture>
                <div
                    className={styles.circleAstre}
                    onClick={() => router.replace("/notice-end-fonction")}
                >
                    {/* <img src={otherChapters.astre.img} /> */}
                    <img src={require("./imgs/blue-circle.png")} />

                    <p>Le fonctionnement</p>
                </div>
                <div
                    className={styles.circleTopRight}
                    onClick={() => router.replace("/notice-end-machine")}
                >
                    <img src={require("./imgs/blue-circle.png")} />
                    <p>Une machine <br></br> à calculer</p>
                </div>
                <div
                    className={styles.circleleftBottom}
                    onClick={() => router.replace("/notice-end-money")}
                >
                    <img src={require("./imgs/blue-circle.png")} />
                    <p>Le système monétaire</p>
                </div>
                <div className={styles.test}></div>
            </div>
            <Header>
                <BackButton
                    onClick={() => router.replace(`/is-end-menu`)}
                ></BackButton>
            </Header>
        </GlobalWrapper>
    );
};

export default NoticeMission1Menu;
