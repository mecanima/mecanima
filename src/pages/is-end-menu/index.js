import React, { useState } from "react";
import styles from "./is-end-menu.module.scss";
import BackButton from "../../comps/back-button";
import GlobalWrapper from "../../comps/globalWrapper";
import HelpBtn from "../../comps/help-btn";
import NoticeBtn from "../../comps/notice-btn";
import FoundBtn from "../../comps/found-btn";
import MissionDescription from "../../comps/mission-description";
import Header from "../../comps/header";
import Container from "../../comps/container/container";
import { useRouter } from "next/router";
import RoomStore from "../../store/room/index";

const IsEndMenu = () => {
    const router = useRouter();

    const [appear, setAppear] = useState(true);
    const handleAppear = () => {
        setAppear(false);
        RoomStore.setPopInOff("third");
        // console.log("second")
    };

    return (
        <>
            <section
                className={`${styles.pop_in} ${appear && !RoomStore.thirdPopIn ? "" : styles.appear}`}
                onClick={handleAppear}
            >
                <div className={styles.inside__Block}>
                    <picture>
                        <source
                            srcSet={require("./background.png?webp")}
                            type="image/webp"
                        />
                        <source
                            srcSet={require("./background.png")}
                            type="image/png"
                        />
                        <img
                            className={styles.background_Img}
                            src={require("./background.png")}
                        />
                    </picture>
                    <p>
                        Pour cette dernière étape, vous devez utiliser
                        uniquement la maquette tangible devant vous.
                    </p>
                    <p>
                        Attention, ne tentez ni un calcul mental ni
                        l’utilisation d’une calculette ! Si les esprits s’en
                        rendent compte, les conséquences pourraient être
                        dramatiques pour toute votre équipe...
                    </p>
                    <BackButton onClick={() => setAppear(false)}></BackButton>
                </div>
            </section>
            <GlobalWrapper beige>
                <MissionDescription
                    missionNum="CODE"
                    noMission={true}
                    title="Trouver le code du cadenas"
                    desc="Votre code est la somme de toutes les missions effectuées"
                ></MissionDescription>
                <div className={styles.containerMission}>
                    <div className={styles.missionBtn}>
                        <img src={require("./fond.png")} />
                        <p>
                            Mission 1 <span>17</span>
                        </p>
                    </div>
                    <h4 className={styles.separator}>+</h4>
                    <div className={styles.missionBtn}>
                        <img src={require("./fond.png")} />
                        <p>
                            Mission 2 <span>285</span>
                        </p>
                    </div>
                    <h4 className={styles.separator}>+</h4>
                    <div className={styles.missionBtn}>
                        <img src={require("./fond.png")} />
                        <p>
                            Mission 3 <span>1492</span>
                        </p>
                    </div>
                    <h4 className={styles.separator}>=</h4>
                    <div className={styles.pascaline}>
                        <img src={require("./pascaline.png")} />
                        <div className={styles.background}>
                            <p>?</p>
                            <img src={require("./background-circle.png")} />
                        </div>
                    </div>
                </div>
            </GlobalWrapper>
            <Header>
                <NoticeBtn
                    onClick={() => router.push("/notice-end-menu")}
                ></NoticeBtn>
                <HelpBtn></HelpBtn>
                <FoundBtn
                    onClick={() => router.push("/is-end-code")}
                ></FoundBtn>
            </Header>
        </>
    );
};

export default IsEndMenu;
