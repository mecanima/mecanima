import React, { useEffect, useRef } from "react";
import styles from "./notice-end-fonction.module.scss";
import NoticeHeader from "../../comps/notice-header";
import OtherChapter from "../../comps/other-chapter";
import Header from "../../comps/header";
import ArrowBtn from "../../comps/arrow-btn";
import GlobalWrapper from "../../comps/globalWrapper";
import { useRouter } from "next/router";
import { useIntersection } from "react-use";
import VideoBackground from "../../comps/videoBackground";

import { otherChapters } from "../../utils/otherChapters";

export const resources = {
    videos: { 
        fonction1: require("./imgs/fonction1.mp4"),
        fonction2: require("./imgs/fonction2.mp4") 
    },
    images: {
    },
};
const NoticeEndFontion = () => {
    const router = useRouter();

    let isSecondAppear = false;
    let isChapterAppear = false;
    const secondText = useRef(null);
    const Chapter = useRef(null);
    const video = useRef(null);
    const sectionVideo = useRef(null);

    const secondIntersection = useIntersection(secondText, {
        root: null,
        rootMargin: "0px",
        threshold: 1,
    });
    const chapterIntersection = useIntersection(Chapter, {
        root: null,
        rootMargin: "0px",
        threshold: 0.5,
    });
    const videoIntersection = useIntersection(sectionVideo, {
        root: null,
        rootMargin: "0px",
        threshold: 1,
    });

    if (video.current !== null)
        videoIntersection && videoIntersection.intersectionRatio < 1
            ? video.current.pause()
            : video.current.play();
    secondIntersection && secondIntersection.intersectionRatio < 1
        ? (isSecondAppear = true)
        : null;
    chapterIntersection && chapterIntersection.intersectionRatio < 0.5
        ? null
        : (isChapterAppear = true);

    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    return (
        <GlobalWrapper beige>
            <div className={styles.noticeContainer}>
                <NoticeHeader tool="Le fonctionne- ment" main={false}></NoticeHeader>
                <div className={`${styles.sectionContainer} ${styles.mt}`}>
                    <VideoBackground
                        loop={false}
                        playsInline
                        autoPlay
                        url={resources.videos.fonction1}
                        className={styles.oculaire}
                    ></VideoBackground>
                    <div className={styles.textContainer}>
                        <p className={styles.firstAnimText}>
                            Cette machine <span className={styles.fontW2}>additionne</span> et <span className={styles.fontW2}>soustrait</span> en effectuant
                            <span className={styles.fontW2}> automatiquement la retenue</span>. Cela est possible grâce à l’aide d’un ingénieux
                            système mécanique basé sur une pièce appelée le <span className={styles.fontW2}>sautoir</span>.
                        </p>
                    </div>
                </div>
                <div className={styles.sectionContainer} ref={sectionVideo}>
                    <VideoBackground
                        loop={false}
                        playsInline
                        autoPlay={false}
                        url={resources.videos.fonction2}
                        ref={video}
                        className={styles.oculaire}
                    ></VideoBackground>
                    <div className={styles.textContainer}>
                        <p
                            ref={secondText}
                            className={isSecondAppear ? "" : styles.animText}
                        >
                            Pour effectuer un calcul, assurez-vous d’avoir les <span className={styles.fontW2}>compteurs à zéro</span>.
                            Puis, placez votre doigt sur le chiffre que vous souhaitez rentrer,
                            et faites le glisser jusqu’au zéro. 
                        </p>
                    </div>
                </div>
                <div ref={Chapter}>
                    <OtherChapter
                        chapter1={otherChapters.money}
                        chapter2={otherChapters.machine}
                        appear={isChapterAppear}
                    ></OtherChapter>
                </div>
                <Header left>
                    <ArrowBtn
                        onClick={() => {
                            router.replace("/notice-end-menu");
                        }}
                    ></ArrowBtn>
                </Header>
            </div>
        </GlobalWrapper>
    );
};

export default NoticeEndFontion;
