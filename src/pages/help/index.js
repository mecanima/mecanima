import React, { useState, useEffect } from "react";
import styles from "./help.module.scss";
import Container from "comps/container/container";
import Header from "comps/header";
import BackButton from "comps/back-button";
import GlobalWrapper from "comps/globalWrapper";
import Hint from "comps/hint/";
import { useObserver } from "mobx-react-lite";
import RoomStore from "store/room";
import { useRouter } from "next/router";

const Help = () => {
    const router = useRouter();

    const [translate, setTranslate] = useState(
        (RoomStore.hintsData.hintsPoints / RoomStore.hintsData.maxHintsPoints) *
            100 -
            100
    );

    useEffect(() => {
        setTranslate(
            (RoomStore.hintsData.hintsPoints /
                RoomStore.hintsData.maxHintsPoints) *
                100 -
                100
        );
    }, [RoomStore.hintsData.hintsPoints]);

    return (
        <>
            <Container fullHeight>
                <GlobalWrapper beige={true}>
                    <h1 className={styles.title}>Indices</h1>
                    <section className={styles.hintContainer}>
                        {useObserver(() =>
                            RoomStore.hintsData.hints.map((hint, index) => (
                                <Hint
                                    index={index}
                                    key={index}
                                    point={hint.cost}
                                    active={
                                        hint.cost <=
                                        RoomStore.hintsData.hintsPoints
                                    }
                                    hint={hint.text || ""}
                                    revealed={hint.hidden === false}
                                ></Hint>
                            ))
                        )}
                    </section>
                    <section className={styles.pointsContainer}>
                        <h4>Nos points</h4>
                        <aside>
                            {useObserver(() => (
                                <span className={styles.pointsCounter}>
                                    {RoomStore.hintsData.hintsPoints} /{" "}
                                    {RoomStore.hintsData.maxHintsPoints}
                                </span>
                            ))}
                            <div className={styles.bar}>
                                <img
                                    className={styles.img}
                                    src={require("./imgs/INDICES_BARRE_FOND.png?resize&size=700")}
                                />
                                <span
                                    className={styles.points}
                                    style={{
                                        transform: `translateX(${translate}%)`,
                                    }}
                                ></span>
                                {/* <img
                            className={styles.points}
                            src={require("./imgs/INDICES_BARRE_LIQUIDE.png?resize&size=700")}
                            style={{ transform: `translateX(${translate}%)` }}
                        /> */}
                                <img
                                    className={styles.graduations}
                                    src={require("./imgs/INDICES_BARRE_GRADUATIONS.png?resize&size=700")}
                                />
                            </div>
                        </aside>
                    </section>
                </GlobalWrapper>
            </Container>
            <Header>
                <BackButton
                    onClick={
                        () => router.back()
                        // RoomStore.secondPopIn
                        //     ? router.replace("/is-tools-menu")
                        //     : router.replace("/is-mission1-intro")
                    }
                ></BackButton>
            </Header>
        </>
    );
};

export default Help;
