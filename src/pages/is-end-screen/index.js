import React from "react";
import styles from "./end-screen.module.scss";
import VideoBackground from "../../comps/videoBackground";

export const resources = {
    videos: { end: require("./end.mp4") },
};

const EndScreen = () => {
    return (
        <div className={styles.end}>
            <VideoBackground
                loop={false}
                playsInline
                autoPlay
                url={resources.videos.end}
                className={styles.videoScreen}
            ></VideoBackground>
        </div>
    );
};

export default EndScreen;
