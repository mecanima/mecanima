import React from "react";
import { useRouter } from "next/router";
import styles from "./is-end-pres.module.scss";
import Container from "comps/container/container";
import Button from "comps/button";

import RoomStore from "store/room";

const IsEndPres = () => {
    const router = useRouter();

    const next = () => {
        RoomStore.setGameState("end-menu");
        router.push(`/is-end-menu`);
    };

    return (
        <>
            <Container fullHeight>
                <div className={styles.presContainer}>
                    <picture>
                        <source
                            srcSet={require("./background.png?webp")}
                            type="image/webp"
                        />
                        <source
                            srcSet={require("./background.png")}
                            type="image/png"
                        />
                        <img
                            className={styles.img}
                            src={require("./background.png")}
                        />
                    </picture>
                    <picture>
                        <source
                            srcSet={require("./pascaline.png?webp")}
                            type="image/webp"
                        />
                        <source
                            srcSet={require("./pascaline.png")}
                            type="image/png"
                        />
                        <img
                            className={styles.img}
                            src={require("./pascaline.png")}
                        />
                    </picture>
                </div>
                <div className={styles.presDescription}>
                    <h4 className={styles.title}>
                        <p className={styles.title_bold}>Pascaline</p>
                        <p className={styles.scientist}>Pascal</p>
                    </h4>
                    <p className={styles.text}>
                        La sortie est proche.<br></br>A la clé, une nuit sereine
                        et le bon<br></br>
                        air de l'extérieur. <br></br>
                        Accrochez-vous pour le calcul final !
                    </p>
                </div>
                <Button
                    text="C'est parti !"
                    // abs={true}
                    onClick={next}
                ></Button>
            </Container>
        </>
    );
};

export default IsEndPres;
