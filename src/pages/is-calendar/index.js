import React, { useState, useEffect, useRef } from "react";
import { useRouter } from "next/router";

import Header from "../../comps/header";
import ArrowBtn from "../../comps/arrow-btn";
import HelpBtn from "../../comps/help-btn";
import FoundBtn from "../../comps/found-btn";
import MissionDescription from "../../comps/mission-description";
import CalendarMoon from "../../comps/calendarMoon";
import GlobalWrapper from "../../comps/globalWrapper";

import { moons } from "../../utils/moons";

import styles from "./calendar.module.scss";

import Flickity from "react-flickity-component";
import dynamic from "next/dynamic";

import Moons from "../../comps/moons";

import RoomStore from "../../store/room";
import { useObserver } from "mobx-react-lite";

const ReactTextTransition = dynamic(() => import("react-text-transition"), {
    ssr: false,
});

const flickityOptions = {
    wrapAround: true,
    prevNextButtons: false,
    pageDots: false,
    initialIndex: 4,
    selectedAttraction: 0.2,
    friction: 0.8,
};

function usePrevious(value) {
    const ref = useRef();
    useEffect(() => {
        ref.current = value;
    });
    return ref.current;
}

const Calendar = () => {
    const router = useRouter();
    // let test = false;
    let isSelected = moons[flickityOptions.initialIndex];
    const flickity = useRef(null);
    const [currentSlide, setCurrentSlide] = useState(
        flickityOptions.initialIndex
    );
    // const oldIndex = usePrevious(currentSlide);
    // const [transitionSide, setTransitionSide] = useState("down");
    useEffect(() => {
        const previousData = RoomStore.tools.find(
            (elm) => elm.name === "calendar"
        );
        const index =
            previousData && typeof previousData.sentAnswer === "number"
                ? previousData.sentAnswer
                : 4;
        setCurrentSlide(index);
        flickityOptions.initialIndex = index;
    }, []);
    useEffect(() => {
        // if (currentSlide === moons.length - 1 && oldIndex === 0) {
        //     setTransitionSide("down");
        // } else if (currentSlide === 0 && oldIndex === moons.length - 1) {
        //     setTransitionSide("up");
        // } else if (currentSlide > oldIndex) {
        //     setTransitionSide("up");
        // } else if (currentSlide < oldIndex) {
        //     setTransitionSide("down");
        // }
        isSelected = moons[currentSlide]; //La valeur a envoyer
        RoomStore.updateToolData("calendar", isSelected.id);
    }, [currentSlide]);
    const setupFlickity = (ref) => {
        flickity.current = ref;
        flickity.current.on("change", (index) => {
            setCurrentSlide(index);
        });
    };

    const throwUpTool = () => {
        RoomStore.pickTool("calendar", false);
        router.replace("/is-tools-menu");
    };

    return useObserver(() => (
        <>
            <GlobalWrapper beige>
                <MissionDescription
                    missionNum="1"
                    desc="Vous êtes le maître du temps"
                ></MissionDescription>
                <style global jsx>{`
                    .is-selected {
                        transform: scale(1.4);
                    }
                `}</style>
                <div className={styles.carouselWrapper}>
                    <Flickity
                        className={styles.carousel}
                        options={flickityOptions}
                        flickityRef={setupFlickity}
                        static
                        disableImagesLoaded
                    >
                        {moons.map((elm) => (
                            <div key={elm.id} className={styles.slide}>
                                <CalendarMoon
                                    selected={currentSlide}
                                    imgSrc={elm.image.big}
                                    moon={elm.id}
                                ></CalendarMoon>
                            </div>
                        ))}
                    </Flickity>
                </div>
                <aside className={styles.moonText}>
                    <Moons moonSelected={currentSlide}></Moons>
                </aside>
                <p className={styles.state}>
                    {(currentSlide === 3 ||
                        currentSlide === 4 ||
                        currentSlide === 5) &&
                        `Eh, mais vous avez décidé de m'aveugler ?`}
                    {(currentSlide === 0 ||
                        currentSlide === 1 ||
                        currentSlide === 7) &&
                        `On voit rien, il fait beaucoup trop noir !`}
                    {currentSlide === 6 &&
                        `Mhh, ça me dit quelque chose, mais je suis pas trop sûr...`}
                    {currentSlide === 2 &&
                        `Celle-là me dit quelque chose en effet...`}
                </p>
                <Header>
                    <ArrowBtn onClick={throwUpTool}></ArrowBtn>
                    <HelpBtn
                        onClick={() => RoomStore.pickTool("calendar", false)}
                        replace
                    ></HelpBtn>
                    <FoundBtn
                        beforeClick={() =>
                            RoomStore.pickTool("calendar", false)
                        }
                    ></FoundBtn>
                </Header>
            </GlobalWrapper>
        </>
    ));
};

// const ObserverRoom = observer(Room);
// const RoomComponent = () => <ObserverRoom users={RoomStore.users} />;

export default Calendar;
