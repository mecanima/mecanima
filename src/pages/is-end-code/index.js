import React, { useState } from "react";
import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import { useObserver } from "mobx-react-lite";

import RoomStore from "store/room";
import UserStore from "store/user";

import styles from "./is-end-code.module.scss";
import BackButton from "../../comps/back-button";
import GlobalWrapper from "../../comps/globalWrapper";
import Header from "../../comps/header";
import Container from "../../comps/container/container";

const ReactTextTransition = dynamic(() => import("react-text-transition"), {
    ssr: false,
});
const IsEndMenu = () => {
    const router = useRouter();
    // const [number, setNumber] = useState([0, 0, 0, 0]);
    const [selectedNumber, setSelectedNumber] = useState(
        process.env.ENV === "production"
            ? RoomStore.getUserPosition(UserStore.id)
            : 0
    );
    const [transitionSide, setTransitionSide] = useState("down");
    const [isFound, setFound] = useState(false);
    let number = RoomStore.finalChallenge[1].value
        .split("")
        .map((elm) => Number(elm));
    if (RoomStore.finalChallenge[1].value === "") {
        number = [0, 0, 0, 0];
    }

    const sendNumber = (numberArray) => {
        RoomStore.setFinalChallenge(
            1,
            numberArray.reduce((acc, curr) => acc + String(curr)),
            ""
        );
    };

    const handleUp = () => {
        setTransitionSide("down");
        const numberArray = [...number];
        numberArray[selectedNumber] === 9
            ? (numberArray[selectedNumber] = 0)
            : numberArray[selectedNumber]++;
        sendNumber(numberArray);
    };
    const handleDown = () => {
        setTransitionSide("up");
        const numberArray = [...number];
        numberArray[selectedNumber] === 0
            ? (numberArray[selectedNumber] = 9)
            : numberArray[selectedNumber]--;
        sendNumber(numberArray);
    };

    useObserver(() => {
        if (RoomStore.finalChallenge[1].value !== "")
            number = RoomStore.finalChallenge[1].value
                .split("")
                .map((elm) => Number(elm));
        if (RoomStore.finalChallenge[1].found === true && isFound === false) {
            setFound(true);
            RoomStore.setGameState("end-screen");
            router.push("/is-end-screen");
        }
    });

    return (
        <>
            <GlobalWrapper beige>
                <Container fullHeight>
                    <img
                        className={styles.arrowUp}
                        src={require("./arrow.png")}
                        onClick={handleUp}
                    />
                    <ReactTextTransition
                        className={styles.textTransition}
                        text={number[selectedNumber]}
                        direction={transitionSide}
                    ></ReactTextTransition>
                    <img
                        className={styles.arrowDown}
                        src={require("./arrow.png")}
                        onClick={handleDown}
                    />
                    <div className={styles.digitsContainer}>
                        {Array(4)
                            .fill(0)
                            .map((elm, index) => (
                                <div
                                    className={`${styles.digit} ${
                                        index === selectedNumber
                                            ? styles.active
                                            : ""
                                    }`}
                                    onClick={() =>
                                        process.env.ENV === "production"
                                            ? null
                                            : setSelectedNumber(index)
                                    }
                                    key={index}
                                >
                                    <img src={require("./background.png")} />
                                    <p>{number[index]}</p>
                                </div>
                            ))}
                    </div>
                </Container>
            </GlobalWrapper>
            <Header>
                <BackButton onClick={router.back}></BackButton>
            </Header>
        </>
    );
};

export default IsEndMenu;
