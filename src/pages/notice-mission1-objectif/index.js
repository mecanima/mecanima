import React, { useEffect, useRef } from "react";
import styles from "./notice-mission1-objectif.module.scss";
import NoticeHeader from "../../comps/notice-header";
import OtherChapter from "../../comps/other-chapter";
import Header from "../../comps/header";
import { useRouter } from "next/router";
import ArrowBtn from "../../comps/arrow-btn";
import GlobalWrapper from "../../comps/globalWrapper";
import { useIntersection } from "react-use";
import VideoBackground from "../../comps/videoBackground";

import { otherChapters } from "../../utils/otherChapters";

export const resources = {
    videos: {
        objectif1: require("./imgs/objectif1.mp4"),
        objectif2: require("./imgs/objectif2.mp4"),
    },
    images: {},
};

const NoticeMission1Objectif = () => {
    const router = useRouter();

    let isSecondAppear = false;
    let isChapterAppear = false;
    const secondText = useRef(null);
    const Chapter = useRef(null);
    const video = useRef(null);
    const sectionVideo = useRef(null);

    const secondIntersection = useIntersection(secondText, {
        root: null,
        rootMargin: "0px",
        threshold: 1,
    });
    const chapterIntersection = useIntersection(Chapter, {
        root: null,
        rootMargin: "0px",
        threshold: 0.5,
    });
    const videoIntersection = useIntersection(sectionVideo, {
        root: null,
        rootMargin: "0px",
        threshold: 1,
    });

    if (video.current !== null)
        videoIntersection && videoIntersection.intersectionRatio < 1
            ? video.current.pause()
            : video.current.play();
    secondIntersection && secondIntersection.intersectionRatio < 1
        ? (isSecondAppear = true)
        : null;
    chapterIntersection && chapterIntersection.intersectionRatio < 0.5
        ? null
        : (isChapterAppear = true);

    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    return (
        <GlobalWrapper beige>
            <div className={styles.noticeContainer}>
                <NoticeHeader tool="L'Objectif" main={false}></NoticeHeader>
                <div className={`${styles.sectionContainer} ${styles.mt}`}>
                    <VideoBackground
                        loop={false}
                        playsInline
                        autoPlay={true}
                        url={resources.videos.objectif1}
                        className={styles.oculaire}
                    ></VideoBackground>
                    <div className={styles.textContainer}>
                        <p className={styles.firstAnimText}>
                            Les découvertes de cette lunette sont dues{" "}
                            <span className={styles.oneLine}>
                                aux{" "}
                                <span className={styles.fontW1}>verres</span>
                                <span className={styles.fontW2}>
                                    {" "}
                                    grossissants
                                </span>
                            </span>{" "}
                            en provenance de Murano. Cette lunette pouvait
                            agrandir jusqu’à{" "}
                            <span className={styles.fontW2}>20 fois</span> un
                            objet observé.
                        </p>
                    </div>
                </div>
                <div className={styles.sectionContainer} ref={sectionVideo}>
                    <VideoBackground
                        loop={false}
                        playsInline
                        autoPlay={false}
                        url={resources.videos.objectif2}
                        className={styles.oculaire}
                        ref={video}
                    ></VideoBackground>
                    <div className={styles.textContainer}>
                        <p
                            ref={secondText}
                            className={isSecondAppear ? "" : styles.animText}
                        >
                            Pour fonctionner elle était constituée d’une{" "}
                            <span className={styles.fontW1}>
                                lentille convergente
                            </span>{" "}
                            <br></br> et d'une{" "}
                            <span className={styles.fontW1}>
                                lentille divergente
                            </span>
                            .
                        </p>
                    </div>
                </div>
                <div ref={Chapter}>
                    <OtherChapter
                        chapter1={otherChapters.astre}
                        chapter2={otherChapters.oculaire}
                        appear={isChapterAppear}
                    ></OtherChapter>
                </div>
                <Header left>
                    <ArrowBtn
                        onClick={() => {
                            router.replace("/notice-mission1-menu");
                        }}
                    ></ArrowBtn>
                </Header>
            </div>
        </GlobalWrapper>
    );
};

export default NoticeMission1Objectif;
