import React, { useEffect, useRef } from "react";
import styles from "./notice-end-machine.module.scss";
import NoticeHeader from "../../comps/notice-header";
import OtherChapter from "../../comps/other-chapter";
import Header from "../../comps/header";
import { useRouter } from "next/router";
import ArrowBtn from "../../comps/arrow-btn";
import GlobalWrapper from "../../comps/globalWrapper";
import { useIntersection } from "react-use";
import VideoBackground from "../../comps/videoBackground";

import { otherChapters } from "../../utils/otherChapters";

export const resources = {
    videos: {
        machine1: require("./imgs/machine1.mp4"),
        machine2: require("./imgs/machine2.mp4"),
    },
    images: {},
};

const NoticeEndMachine = () => {
    const router = useRouter();

    let isSecondAppear = false;
    let isChapterAppear = false;
    const secondText = useRef(null);
    const Chapter = useRef(null);
    const video = useRef(null);
    const sectionVideo = useRef(null);

    const secondIntersection = useIntersection(secondText, {
        root: null,
        rootMargin: "0px",
        threshold: 1,
    });
    const chapterIntersection = useIntersection(Chapter, {
        root: null,
        rootMargin: "0px",
        threshold: 0.5,
    });
    const videoIntersection = useIntersection(sectionVideo, {
        root: null,
        rootMargin: "0px",
        threshold: 1,
    });

    if (video.current !== null)
        videoIntersection && videoIntersection.intersectionRatio < 1
            ? video.current.pause()
            : video.current.play();
    secondIntersection && secondIntersection.intersectionRatio < 1
        ? (isSecondAppear = true)
        : null;
    chapterIntersection && chapterIntersection.intersectionRatio < 0.5
        ? null
        : (isChapterAppear = true);

    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    return (
        <GlobalWrapper beige>
            <div className={styles.noticeContainer}>
                <NoticeHeader tool="Une machine à calculer" main={false}></NoticeHeader>
                <div className={`${styles.sectionContainer} ${styles.mt}`}>
                    <VideoBackground
                        loop={false}
                        playsInline
                        autoPlay={true}
                        url={resources.videos.machine1}
                        className={styles.oculaire}
                    ></VideoBackground>
                    <div className={styles.textContainer}>
                        <p className={styles.firstAnimText}>
                            Sur la véritable pascaline les 2 roues de droite avaient 20 et 12 dents. Il y avait 12 deniers dans un sol.
                        </p>
                    </div>
                </div>
                <div className={styles.sectionContainer} ref={sectionVideo}>
                    <VideoBackground
                        loop={false}
                        playsInline
                        autoPlay={false}
                        url={resources.videos.machine2}
                        className={styles.oculaire}
                        ref={video}
                    ></VideoBackground>
                    <div className={styles.textContainer}>
                        <p
                            ref={secondText}
                            className={isSecondAppear ? "" : styles.animText}
                        >
                           Et il y avait 20 sols dans une livre pour 
                            correspondre au <span className={styles.fontW2}>système monétaire </span> de l’époque. 
                        </p>
                    </div>
                </div>
                <div ref={Chapter}>
                    <OtherChapter
                        chapter1={otherChapters.money}
                        chapter2={otherChapters.fonction}
                        appear={isChapterAppear}
                    ></OtherChapter>
                </div>
                <Header left>
                    <ArrowBtn
                        onClick={() => {
                            router.replace("/notice-end-menu");
                        }}
                    ></ArrowBtn>
                </Header>
            </div>
        </GlobalWrapper>
    );
};

export default NoticeEndMachine;
