import React, { useEffect, useRef, useState, Suspense } from "react";
import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import Container from "../../comps/container/container";
import Header from "../../comps/header";
import ArrowBtn from "../../comps/arrow-btn";
import HelpBtn from "../../comps/help-btn";
import FoundBtn from "../../comps/found-btn";
import MissionDescription from "../../comps/mission-description";
import GlobalWrapper from "../../comps/globalWrapper";

import styles from "./telescope.module.scss";

import { startCompass, stopCompass } from "../../utils/compass";

import { useObserver } from "mobx-react-lite";
import RoomStore from "../../store/room";

import { Canvas } from "react-three-fiber";
const TelescopeCanvas = dynamic(() => import("comps/telescope"), {
    ssr: false,
});

const calibrationImage = require("./calibration.png");

const Telescope = () => {
    const router = useRouter();
    const throwUpTool = () => {
        RoomStore.pickTool("telescope", false);
        router.replace("/is-tools-menu");
    };

    const [moon, setMoon] = useState(0);
    const [startViewer, setStartViewer] = useState(null);

    const [currentOrientation, setCurrentOrientation] = useState({
        alpha: 0,
        beta: 0,
        gamma: 0,
    });
    const [blur, setBlur] = useState("");
    const panoContainer = useRef(null);
    const handleDirection = (orientation) => {
        setCurrentOrientation(orientation);
    };

    let calendar;
    useObserver(
        () =>
            (calendar = RoomStore.tools.find((elm) => elm.name === "calendar"))
    );
    let lenses;
    useObserver(
        () => (lenses = RoomStore.tools.find((elm) => elm.name === "lenses"))
    );
    const startTelescope = () => {
        startCompass(handleDirection);
        setStartViewer(true);
    };
    const resetTelescope = () => {
        handleDirection(currentOrientation);
    };
    useEffect(() => {
        setMoon(calendar && calendar.sentAnswer);
    }, [calendar && calendar.sentAnswer]);
    useEffect(() => {
        let startBlur = 10;
        if (lenses && lenses.sentAnswer[0] === "convergent") startBlur -= 5;
        if (lenses && lenses.sentAnswer[1] === "divergent") startBlur -= 5;
        setBlur(startBlur);
    }, [lenses && lenses.sentAnswer]);
    useEffect(() => () => stopCompass(), []);

    const cameraFov = process.env.ENV === "production" ? 10 : 10;

    return (
        <>
            <GlobalWrapper beige>
                <MissionDescription
                    missionNum="1"
                    desc="Vous êtes l'astronome"
                ></MissionDescription>
                <Container fullHeight>
                    <section
                        className={styles.telescopeContainer}
                        ref={panoContainer}
                        onClick={startTelescope}
                        onDoubleClick={resetTelescope}
                    >
                        <Canvas
                            noEvents
                            camera={{ fov: cameraFov }}
                            gl={{ alpha: true }}
                            pixelRatio={window.devicePixelRatio}
                            style={{ filter: `blur(${blur}px)` }}
                            className={styles.canvas}
                        >
                            <Suspense fallback={null}>
                                <TelescopeCanvas
                                    telescopeState={{
                                        calendar: moon,
                                        lenses: blur === 0,
                                    }}
                                    orientation={currentOrientation}
                                    startViewer={startViewer}
                                ></TelescopeCanvas>
                            </Suspense>
                        </Canvas>
                        {!startViewer && (
                            <div className={styles.calibrationContainer}>
                                <p>
                                    Pour activer l’objectif, mettez votre
                                    téléphone à plat, puis cliquez 1 fois dans
                                    cette zone.
                                </p>
                                <img src={calibrationImage}></img>
                            </div>
                        )}
                    </section>
                    <p className={styles.text}>
                        {blur >= 5 &&
                            moon !== 2 &&
                            `Tant que vos coéquipiers n’auront pas choisi quel astre
                        observer, dans quelle direction et avec quel matériel,
                        vous n’êtes pas prêts d’admirer la Lune.`}
                        {blur === 0 &&
                            (moon === 3 || moon === 4 || moon === 5) &&
                            `La lune est difficile à voir avec tant de lumière... Qui a décidé de choisir ce moment ?`}
                        {blur === 0 &&
                            (moon === 0 || moon === 1 || moon === 7) &&
                            `La lune est difficile à voir avec si peu de lumière... Qui a décidé de choisir ce moment ?`}
                        {blur !== 0 &&
                            moon === 2 &&
                            `On dirait que c'est la lune qu'on cherche, mais impossible de voir sans les bonnes lentilles. Un peu de nerfs, l'opticien !`}
                        {blur === 0 &&
                            moon === 2 &&
                            `J'arrive à apercevoir quelque chose sur la lune en plissant les yeux...`}
                    </p>
                </Container>
                <Header>
                    <ArrowBtn onClick={throwUpTool}></ArrowBtn>
                    <HelpBtn
                        onClick={() => RoomStore.pickTool("telescope", false)}
                        replace
                    ></HelpBtn>
                    <FoundBtn
                        beforeClick={() => RoomStore.pickTool("telescope", false)}
                    ></FoundBtn>
                </Header>
            </GlobalWrapper>
        </>
    );
};

export default Telescope;
