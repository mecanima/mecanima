import React, { useState, useEffect } from "react";
import { useObserver } from "mobx-react-lite";
import { useRouter } from "next/router";
import styles from "./compass.module.scss";

import { moons, moonLocations } from "../../utils/moons";
import Container from "../../comps/container/container";
import Header from "../../comps/header";
import ArrowBtn from "../../comps/arrow-btn";
import HelpBtn from "../../comps/help-btn";
import FoundBtn from "../../comps/found-btn";
import MissionDescription from "../../comps/mission-description";
import GlobalWrapper from "../../comps/globalWrapper";

import { startCompass, stopCompass } from "../../utils/compass";

import RoomStore from "../../store/room";

import { debounce } from "lodash";

const debouncedSend = debounce(
    (rotation) => RoomStore.updateToolData("compass", rotation),
    250,
    { maxWait: 500 }
);

const Compass = () => {
    const router = useRouter();
    const [rotation, setRotation] = useState(0);
    const [sticky, setSticky] = useState(false);

    const throwUpTool = () => {
        router.replace("/is-tools-menu");
    };

    const sendData = (rotation) => {
        debouncedSend(rotation);
        setRotation(rotation);
    };

    const handleAbsoluteOrientation = (orientation) => {
        const yaw = orientation.alpha;
        sendData(yaw);
        // } else {
        //     alert(
        //         "Votre téléphone ne peut pas récupérer les données de la boussole. Merci de réessayer avec Google Chrome ou Safari sur iOS."
        //     );
        //     throwUpTool();
        // }
    };

    const initializeDataRetrieval = () => {
        startCompass(handleAbsoluteOrientation);
    };

    useEffect(() => {
        return () => {
            stopCompass();
            // Seems to trigger when it should not
            // debouncedSend.flush();
        };
    }, []);

    initializeDataRetrieval();

    let calendar;

    useObserver(
        () =>
            (calendar = RoomStore.tools.find((elm) => elm.name === "calendar"))
    );

    const moonRotation =
        rotation + moonLocations[calendar && calendar.sentAnswer];
    useEffect(() => {
        if (
            (moonRotation % 360 > 340 || moonRotation % 360 < 20) &&
            sticky !== false
        ) {
            return;
        }
        if (moonRotation % 360 > 340) {
            setSticky(styles.stickToTopLeft);
        } else if (moonRotation % 360 < 20) {
            setSticky(styles.stickToTopRight);
        } else {
            setSticky(false);
        }
    });

    return (
        <>
            <GlobalWrapper beige>
                <MissionDescription
                    missionNum="1"
                    desc="Vous êtes le maître de l'orientation"
                ></MissionDescription>
                <Container fullHeight>
                    <div
                        className={styles.compassContainer}
                        onClick={initializeDataRetrieval}
                    >
                        {/* <img
                            className={styles.aiguille}
                            src={require("./imgs/aiguille.png")}
                        /> */}
                        <span
                            className={`${styles.moon} ${
                                sticky !== false
                                    ? sticky + " " + styles.sticky
                                    : ""
                            }`}
                            style={{
                                transform: `rotate(${moonRotation % 360}deg)`,
                            }}
                        >
                            <img
                                src={
                                    moons[calendar && calendar.sentAnswer] &&
                                    moons[calendar && calendar.sentAnswer].image
                                        .thumb
                                }
                            />
                        </span>
                        <img
                            className={styles.compass}
                            style={{ transform: `rotate(${rotation}deg)` }}
                            src={require("./imgs/boussole.png")}
                        />
                    </div>

                    <h4 className={styles.deg}>{Math.round(rotation)}°</h4>
                    <h6 className={styles.text}>
                        {calendar && calendar.sentAnswer
                            ? moons[calendar.sentAnswer].step
                            : moons[0].step}
                    </h6>

                    <p className={styles.state}>
                        {sticky !== false && "Bon cap !"}
                        {!sticky &&
                            "Je ne sais pas ce que vous faites en regardant ici, mais ça ne va pas nous aider..."}
                    </p>
                </Container>
                <Header>
                    <ArrowBtn onClick={throwUpTool}></ArrowBtn>
                    <HelpBtn
                        onClick={() => RoomStore.pickTool("compass", false)}
                        replace
                    ></HelpBtn>
                    <FoundBtn
                        beforeClick={() => RoomStore.pickTool("compass", false)}
                    ></FoundBtn>
                </Header>
            </GlobalWrapper>
        </>
    );
};

export default Compass;
