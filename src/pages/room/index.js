import React from "react";
import { useObserver } from "mobx-react-lite";
import ImagePreloader from "image-preloader";

import styles from "./room.module.scss";
import Container from "comps/container/container";
import GlobalWrapper from "comps/globalWrapper";
import Button from "comps/button";
import User from "comps/user";
import RoomStore from "store/room";
import UserStore from "store/user";
import { useRouter } from "next/router";
import { getRoomRoute } from "utils/route";

import { images as nextImages } from "pages/is-starting-screen";

let preloader;
if (typeof window !== "undefined") {
    preloader = new ImagePreloader();
    preloader.preload(Object.values(nextImages));
}

const Room = () => {
    const userId = UserStore.id;
    const otherUsersInRoom = RoomStore.getOtherUsers(userId);
    const user = RoomStore.getUser(userId);
    const otherNames = RoomStore.getOtherNames();
    // console.log(otherNames);
    const router = useRouter();
    const notConnected = 3 - otherUsersInRoom.length;
    for (let i = 0; i < notConnected; i++) {
        otherUsersInRoom.push(undefined);
    }

    const startRoom = () => {
        const nextLocation = getRoomRoute("room");
        if (nextLocation) {
            router.push(nextLocation);
        } else {
            // setTimeout(() => {
            RoomStore.startRoom();
            router.push(`/${RoomStore.gameName}-starting-screen`);
            // }, 2000);
        }
    };

    return useObserver(() => (
        <div>
            <GlobalWrapper beige={true}>
                <Container>
                    {user && (
                        <User
                            name={user.character.name}
                            image={user.character.icon}
                            state="currentUser"
                        ></User>
                    )}

                    <div className={styles.otherPlayers}>
                        {otherUsersInRoom.map((elm) =>
                            typeof elm !== "undefined" ? (
                                <User
                                    key={elm.id}
                                    name={elm.character.name}
                                    state="otherUsers"
                                    image={elm.character.icon}
                                ></User>
                            ) : null
                        )}

                        {otherNames.map((elm) => (
                            <User key={elm} name={elm} state="noUser"></User>
                        ))}
                    </div>

                    {RoomStore.users.length < 4 &&
                    process.env.ENV === "production" ? (
                        <p className={styles.text}>
                            Commencer la partie uniquement lorsque votre groupe
                            est constitué
                        </p>
                    ) : (
                        <>
                            <p className={styles.text}>
                                {/* Vous pouvez commencer la partie ! */}
                                Commencer la partie uniquement lorsque votre
                                groupe est constitué.
                            </p>
                            {/* <button onClick={() => startRoom()}>Commencer</button> */}
                            <Button
                                text="C'est parti !"
                                onClick={startRoom}
                            ></Button>
                        </>
                    )}
                </Container>
            </GlobalWrapper>
        </div>
    ));
};

// const ObserverRoom = observer(Room);
// const RoomComponent = () => <ObserverRoom users={RoomStore.users} />;

export default Room;
