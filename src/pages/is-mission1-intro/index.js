import React, { useState } from "react";
import styles from "./mission1-intro.module.scss";
import Container from "../../comps/container/container";
import { useObserver } from "mobx-react-lite";
import { useRouter } from "next/router";
import FirstQuestion from "../../comps/first-question";
import HelpBtn from "../../comps/help-btn";
import Header from "../../comps/header";
import BackButton from "../../comps/back-button";

import RoomStore from '../../store/room/index';

const MissionMenu = () => {
    const router = useRouter();

    const [appear, setAppear] = useState(true);

    const handleAppear = () => {
        setAppear(false);
        RoomStore.setPopInOff("first");
    }
    return useObserver(() => (
        <>
            <section
                className={`${styles.pop_in} ${appear && !RoomStore.firstPopIn ? "" : styles.appear}`}
                onClick={handleAppear}
            >
                <div className={styles.inside__Block}>
                    <picture>
                        <source
                            srcSet={require("./background.png?webp")}
                            type="image/webp"
                        />
                        <source
                            srcSet={require("./background.png")}
                            type="image/png"
                        />
                        <img
                            className={styles.background_Img}
                            src={require("./background.png")}
                        />
                    </picture>
                    <p>
                        Observez les vitrines à travers toute la collection des
                        instruments scientifiques pour pouvoir répondre à la
                        prochaine question.
                    </p>
                    <BackButton
                        onClick={() => router.replace("/is-mission1-intro")}
                    ></BackButton>
                </div>
            </section>
            <Container fullHeight>
                <div className={styles.mission1_IntroContainer}>
                    <FirstQuestion
                        title="Rendez-vous à la mission 1"
                        text="Rendez-vous dans une ville où de nombreuses découvertes 
                        astronomiques ont changées le cours de l’Histoire."
                        question="Laquelle ?"
                        destination="tool-pres"
                    ></FirstQuestion>
                </div>
            </Container>
            <Header>
                <HelpBtn></HelpBtn>
            </Header>
        </>
    ));
};

// const ObserverRoom = observer(Room);
// const RoomComponent = () => <ObserverRoom users={RoomStore.users} />;

export default MissionMenu;
