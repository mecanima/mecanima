import React, { useRef, useState, useEffect } from "react";
import styles from "./starting-screen.module.scss";
import Container from "../../comps/container/container";
import Button from "../../comps/button";
import GlobalWrapper from "../../comps/globalWrapper";
import MissionBtn from "../../comps/mission-btn";
import { useRouter } from "next/router";
import RoomStore from "../../store/room";
// import lax from "lax.js";

import { useLax, useLaxElement } from "use-lax";
import { useIntersection } from "react-use";
import {
    disableBodyScroll,
    enableBodyScroll,
    clearAllBodyScrollLocks,
} from "body-scroll-lock";

export const images = {
    cloud1: require("./imgs/clouds/cloud1.png?resize&size=200"),
    cloud2: require("./imgs/clouds/cloud2.png?resize&size=200"),
    cloud3: require("./imgs/clouds/cloud3.png?resize&size=200"),
    cloud4: require("./imgs/clouds/cloud4.png?resize&size=200"),
    cloud5: require("./imgs/clouds/cloud5.png?resize&size=200"),
    eglise: require("./imgs/eglise.png?resize&size=720"),
    grille: require("./imgs/grille.png?resize&size=400"),
    tombeau: require("./imgs/tombeau.png?resize&size=300"),
    couvercle: require("./imgs/couvercle.png?resize&size=300"),
    esprit1: require("./imgs/esprit1.png?resize&size=300"),
    esprit2: require("./imgs/esprit2.png?resize&size=300"),
    galerie: require("./imgs/galerie.png?resize&size=1000"),
    plan: require("./imgs/plan.png?resize&size=500"),
    foot1: require("./imgs/foot/foot1.png?resize&size=40"),
    foot2: require("./imgs/foot/foot2.png?resize&size=40"),
    foot3: require("./imgs/foot/foot3.png?resize&size=40"),
    foot4: require("./imgs/foot/foot4.png?resize&size=40"),
    foot5: require("./imgs/foot/foot5.png?resize&size=40"),
    foot6: require("./imgs/foot/foot6.png?resize&size=40"),
    foot7: require("./imgs/foot/foot7.png?resize&size=40"),
    cadre: require("./imgs/part2/cadre.png?resize&size=800"),
    porteGauche: require("./imgs/part2/porte_gauche.png?resize&size=800"),
    porteDroite: require("./imgs/part2/porte_droite.png?resize&size=800"),
    lock: require("./imgs/part2/lock.png?resize&size=500"),
};

//Il manque le placement des textes, transforms ou pas ? A voir

const StartingScreen = () => {
    useLax();
    const ghost1 = useLaxElement();
    const ghost2 = useLaxElement();
    const ghost3 = useLaxElement();
    const ghost4 = useLaxElement();
    const coffin = useLaxElement();
    const couvercle = useLaxElement();

    const body = document.querySelector('body');

    const [veilAppear, setVeilAppear] = useState(false);

    let instrumentsAppear = false;
    const instrumentsText = useRef(null);
    let stairsAppear = false;
    const stairsText = useRef(null);
    let oldSpiritAppear = false;
    const oldSpiritText = useRef(null);
    let fearAppear = false;
    const fearText = useRef(null);
    const ghostText = useRef(null);
    let ghostAppear = false;
    let fourScientist = false;
    const fourScientistText = useRef(null);
    let sectionLockFirst = false;
    const sectionLockFirstText = useRef(null);
    let sectionLockSecond = false;
    const sectionLockSecondText = useRef(null);
    let lockScale = false;
    const sectionLockScale = useRef(null);
    let sectionTomb = false;
    const sectionTombText = useRef(null);
    let sectionCadenas = false;
    const sectionCadenasText = useRef(null);
    let isMissionPicker = false;
    const missionPicker = useRef(null);
    let isRender = false;
    const render = useRef(null);
    const sectionCoffin = useRef(null);

    const instrumentsIntersection = useIntersection(instrumentsText, {
        root: null,
        rootMargin: "0px",
        threshold: 1,
    });
    const stairsIntersection = useIntersection(stairsText, {
        root: null,
        rootMargin: "0px",
        threshold: 1,
    });
    const oldSpiritIntersection = useIntersection(coffin, {
        root: null,
        rootMargin: "0px",
        threshold: 0.8,
    });
    const fearIntersection = useIntersection(ghostText, {
        root: null,
        rootMargin: "0px",
        threshold: 0.1,
    });
    const ghostIntersection = useIntersection(ghost2, {
        root: null,
        rootMargin: "0px",
        threshold: 0.2,
    });
    const fourScientistIntersection = useIntersection(fourScientistText, {
        root: null,
        rootMargin: "0px",
        threshold: 0.5,
    });
    const sectionLockFirstIntersection = useIntersection(sectionLockFirstText, {
        root: null,
        rootMargin: "0px",
        threshold: 1,
    });
    const sectionLockSecondIntersection = useIntersection(
        sectionLockSecondText,
        {
            root: null,
            rootMargin: "0px",
            threshold: 0.5,
        }
    );
    const sectionLockScaleIntersection = useIntersection(sectionLockScale, {
        root: null,
        rootMargin: "0px",
        threshold: 1,
    });
    const sectionTombTextIntersection = useIntersection(sectionTombText, {
        root: null,
        rootMargin: "0px",
        threshold: 1,
    });
    const sectionCadenasTextIntersection = useIntersection(sectionCadenasText, {
        root: null,
        rootMargin: "0px",
        threshold: 1,
    });
    const missionPickerIntersection = useIntersection(missionPicker, {
        root: null,
        rootMargin: "0px",
        threshold: 0.5,
    });
    const renderIntersection = useIntersection(render, {
        root: null,
        rootMargin: "0px",
        threshold: 0.9,
    });

    const startSecondPart = () => {
        setVeilAppear(true);
        console.log("OUAI");
        // veilAppear ? clearAllBodyScrollLocks() : null;
    };

    useEffect(() => {
        clearAllBodyScrollLocks();
    }, [veilAppear]);

    const handleEnableBodyScroll = () => {
        veilAppear ? null : enableBodyScroll(body);
    };
    const handleDisableBodyScroll = () => {
        veilAppear ? null : disableBodyScroll(body);
    };

    oldSpiritIntersection && oldSpiritIntersection.intersectionRatio < 0.8
        ? (oldSpiritAppear = true)
        : null;
    fearIntersection && fearIntersection.intersectionRatio < 0.1
        ? (fearAppear = true)
        : null;
    ghostIntersection && ghostIntersection.intersectionRatio < 0.2
        ? (ghostAppear = true)
        : null;
    fourScientistIntersection &&
    fourScientistIntersection.intersectionRatio < 0.5
        ? (fourScientist = true)
        : null;
    instrumentsIntersection && instrumentsIntersection.intersectionRatio < 1
        ? (instrumentsAppear = true)
        : null;
    stairsIntersection && stairsIntersection.intersectionRatio < 1
        ? (stairsAppear = true)
        : null;
    sectionLockFirstIntersection &&
    sectionLockFirstIntersection.intersectionRatio < 1
        ? (sectionLockFirst = true)
        : null;
    sectionLockScaleIntersection &&
    sectionLockScaleIntersection.intersectionRatio < 1
        ? (lockScale = true)
        : null;
    sectionLockSecondIntersection &&
    sectionLockSecondIntersection.intersectionRatio < 0.5
        ? (sectionLockSecond = true)
        : null;
    sectionTombTextIntersection &&
    sectionTombTextIntersection.intersectionRatio < 1
        ? (sectionTomb = true)
        : null;
    sectionCadenasTextIntersection &&
    sectionCadenasTextIntersection.intersectionRatio < 1
        ? (sectionCadenas = true)
        : null;
    missionPickerIntersection &&
    missionPickerIntersection.intersectionRatio < 0.5
        ? (isMissionPicker = true)
        : null;
    renderIntersection && renderIntersection.intersectionRatio < 0.9
        ? handleEnableBodyScroll()
        : handleDisableBodyScroll()

    const router = useRouter();

    const start = (missionId) => {
        let nextState;
        switch (missionId) {
            case 0:
                nextState = "mission1-intro";
                break;
            case 1:
                nextState = "mission2-intro";
                break;
            case 2:
                nextState = "mission3-intro";
                break;
            case 3:
                nextState = "mission4-intro";
        }
        RoomStore.setGameState(nextState, missionId);
        router.push("/is-" + nextState);
    };

    return (
        <div className={styles.scenario_background}>
            <GlobalWrapper brown={true}>
                <section className={styles.sectionChurch}>
                    <p>
                        Suite à la rénovation du musée en 2000, des{" "}
                        <span className={styles.fontW1}>tombeaux </span>{" "}
                        présents sous
                        <span className={styles.fontW1}>
                            {" "}
                            l’église Saint-Martin
                        </span>{" "}
                        <span className={styles.fontW3}>des-champs </span>
                        <span className={styles.lastwords}>
                            ont été découverts.
                        </span>
                    </p>
                    <div className={styles.clouds}>
                        <img className={styles.cloud1} src={images.cloud1} />
                        <img className={styles.cloud2} src={images.cloud2} />
                        <img className={styles.cloud3} src={images.cloud3} />
                        <img className={styles.cloud4} src={images.cloud4} />
                        <img className={styles.cloud5} src={images.cloud5} />
                    </div>
                    <img className={styles.church} src={images.eglise} />
                    <img className={styles.grilles} src={images.grille} />
                    <div className={styles.blue}></div>
                </section>
                <section className={styles.sectionCoffin} ref={sectionCoffin}>
                    <p
                        className={`${styles.oldSpiritText} ${
                            oldSpiritAppear ? "" : styles.animText
                        }`}
                    >
                        Depuis, <br></br>les collections du Musée des Arts{" "}
                        <br></br>et Métiers sont habitées par de{" "}
                        <span className={styles.fontW1}>vieux esprits...</span>
                    </p>
                    <p
                        className={`${styles.fearText} ${
                            fearAppear ? "" : styles.animText
                        }`}
                    >
                        La <span className={styles.fontW1}>peur</span> s’est
                        emparée des visiteurs et des conservateurs, faisant du
                        lieu,
                    </p>
                    <p
                        className={`${styles.ghostText} ${
                            ghostAppear ? "" : styles.animText
                        }`}
                        ref={ghostText}
                    >
                        <span className={`${styles.fontW1} ${styles.noCap}`}>
                            Un musée
                        </span>{" "}
                        <span className={styles.fontW2}>fantôme.</span>
                    </p>
                    <img
                        ref={coffin}
                        // data-lax-translate-x="0 0, 800 -150, 1200 -300"
                        data-lax-translate-x="90elh (vw*1.5), 70elh (vw*1.5), 0 (-vw*1.5)"
                        data-lax-anchor="self"
                        className={styles.coffin}
                        src={images.tombeau}
                    />
                    <img
                        ref={couvercle}
                        // data-lax-translate-x="0 0, 800 -150, 1200 -300"
                        data-lax-translate-x="90elh (vw*1.5), 70elh (vw*1.5), 0 (-vw*1.5)"
                        data-lax-anchor="self"
                        className={styles.couvercle}
                        src={images.couvercle}
                    />
                    <img
                        ref={ghost1}
                        // data-lax-opacity="90elh 1, 30elh 1, 0 0"
                        data-lax-translate-x="500elh (vw*1.5), 30elh (vw*1.5), 0 (-vw*1.5)"
                        data-lax-translate-y="90elh 0, 30elh 0, 0 -100"
                        data-lax-anchor="self"
                        // data-lax-translate-x="0 0, 600 0, 1600 -600"
                        // data-lax-translate-y="0 0, 600 0, 1200 -100"
                        className={styles.ghost1}
                        src={images.esprit1}
                    />
                    <img
                        ref={ghost2}
                        data-lax-translate-x="500elh (-vw*1.5), 90elh (-vw*1.5), 30elh (vw*2)"
                        data-lax-translate-y="100elh 0, 90elh 0, 30elh -300"
                        data-lax-anchor="self"
                        className={styles.ghost2}
                        src={images.esprit2}
                    />
                </section>
                <section
                    className={`${styles.sectionScientist} ${
                        veilAppear ? styles.renderColor : ""
                    }`}
                >
                    {/* data-lax-translate-y="0 -20" */}
                    <p
                        className={`${styles.firstText} ${
                            fourScientist ? "" : styles.animFirstText
                        }`}
                    >
                        Vous êtes une équipe <br></br>
                        de <span className={styles.fontW2}>
                            {" "}
                            4 chercheurs,
                        </span>{" "}
                        <br></br>bravant la peur pour étudier...
                    </p>
                    <img
                        ref={fourScientistText}
                        className={styles.galerie}
                        src={images.galerie}
                    />
                    <p
                        ref={instrumentsText}
                        className={instrumentsAppear ? "" : styles.animText}
                    >
                        <span className={styles.fontW2}>la galerie</span>
                        <br></br>
                        <span className={styles.fontW2}> des instruments</span>
                        <span className={styles.fontW3}> scientifiques.</span>
                    </p>
                </section>
                <section
                    className={`${styles.render} ${
                        veilAppear ? styles.renderColor : ""
                    }`}
                    ref={render}
                >
                    <p className={stairsAppear ? "" : styles.animText}>
                        Pour vous y rendre, prenez l’ascenceur et{" "}
                        <span className={styles.fontW1}>
                            montez au 2<span className={styles.exp}>ème</span>{" "}
                            étage.
                        </span>
                    </p>
                    <img
                        className={styles.plan}
                        ref={stairsText}
                        src={images.plan}
                    />
                    <div className={styles.foots}>
                        <img
                            className={`${styles.foot1} ${
                                stairsAppear ? "" : styles.appear
                            }`}
                            src={images.foot1}
                        />
                        <img
                            className={`${styles.foot2} ${
                                stairsAppear ? "" : styles.appear
                            }`}
                            src={images.foot2}
                        />
                        <img
                            className={`${styles.foot3} ${
                                stairsAppear ? "" : styles.appear
                            }`}
                            src={images.foot3}
                        />
                        <img
                            className={`${styles.foot4} ${
                                stairsAppear ? "" : styles.appear
                            }`}
                            src={images.foot4}
                        />
                        <img
                            className={`${styles.foot5} ${
                                stairsAppear ? "" : styles.appear
                            }`}
                            src={images.foot5}
                        />
                        <img
                            className={`${styles.foot6} ${
                                stairsAppear ? "" : styles.appear
                            }`}
                            src={images.foot6}
                        />
                        <img
                            className={`${styles.foot7} ${
                                stairsAppear ? "" : styles.appear
                            }`}
                            src={images.foot7}
                        />
                    </div>
                    {/* <Container> */}
                        <div
                            className={`${styles.buttonContainer} ${
                                stairsAppear ? "" : styles.buttonAppear
                            } ${veilAppear ? styles.buttonDisappear : ""}
                            `}
                        >
                            <Button
                                text="On est arrivés !"
                                onClick={() => startSecondPart()}
                                scenario
                            ></Button>
                        </div>
                    {/* </Container> */}
                    <div
                        className={`${styles.veil} ${
                            veilAppear ? styles.veilAppear : ""
                        }`}
                    >
                        <p className={`${veilAppear ? styles.animText : ""}`}>
                            A peine le seuil de la collection franchi.
                        </p>
                        <img src={images.cadre} />
                        <img
                            className={`${styles.leftDoor} ${
                                veilAppear ? styles.leftDoorClose : ""
                            }`}
                            src={images.porteGauche}
                        />
                        <img
                            className={`${styles.rightDoor} ${
                                veilAppear ? styles.rightDoorClose : ""
                            }`}
                            src={images.porteDroite}
                        />
                    </div>
                </section>
                <section className={styles.sectionLock}>
                    <section
                        className={styles.sectionLock__first}
                        ref={sectionLockFirstText}
                    >
                        <p
                            className={`${styles.violent} ${
                                sectionLockFirst ? "" : styles.animTextInOne
                            }`}
                        >
                            Les portes se referment{" "}
                            <span className={styles.fontW2}>violemment</span>
                            <br></br>
                            <span className={styles.behind}>
                                derrière vous.
                            </span>
                        </p>
                        <p
                            className={`${styles.impossible} ${
                                sectionLockFirst ? "" : styles.animTextInTwo
                            }`}
                        >
                            Vous essayez de les rouvrir mais{" "}
                            <span className={styles.fontW2}>impossible.</span>
                        </p>
                    </section>
                    <section className={styles.lockContainer}>
                        <img
                            className={`${styles.lock} ${
                                lockScale ? "" : styles.lockScale
                            } ${lockScale ? "" : styles.secondLockScale}`}
                            src={images.lock}
                        />
                    </section>
                    <section
                        className={styles.sectionLock__second}
                        ref={sectionLockSecondText}
                    >
                        <p
                            className={`${styles.cadenas} ${
                                sectionLockSecond ? "" : styles.animTextInOne
                            }`}
                            ref={sectionLockScale}
                        >
                            Vous remarquez un{" "}
                            <span className={styles.fontW1}>
                                cadenas <br></br>à
                            </span>{" "}
                            <span className={styles.fontW2}>4 chiffres.</span>
                        </p>
                        <p
                            className={`${styles.code} ${
                                sectionLockSecond ? "" : styles.animTextInTwo
                            }`}
                        >
                            Si vous ne trouvez pas{" "}
                            <span className={styles.fontW1}>ce code,</span> vous
                            pourriez bien passer la{" "}
                            <span className={styles.fontW1}>nuit ...</span>
                        </p>
                    </section>
                    <section className={styles.tombGhost} id="tombGhost">
                        <img
                            className={styles.ghost1}
                            ref={ghost3}
                            src={images.esprit1}
                            data-lax-translate-x="(-vh*0.9) (-vw*1.5), (-vh*0.1) (vw)"
                            data-lax-translate-y="(-vh*0.9) 0, (-vh*0.1) 100"
                            data-lax-anchor="self"
                        />
                        <img
                            className={styles.ghost2}
                            ref={ghost4}
                            src={images.esprit2}
                            data-lax-translate-x="(-vh*1.1) (vw*1.5), (-vh*0.6) (-vw)"
                            data-lax-anchor="self"
                        />

                        <p
                            className={`${styles.tomb} ${
                                sectionTomb ? "" : styles.animText
                            }`}
                            ref={sectionTombText}
                        >
                            <span className={styles.fontW1}>En compagnie</span>
                            <br></br>
                            <span className={styles.fontW2}> des esprits</span>
                            <br></br>
                            <span className={styles.fontW3}>
                                {" "}
                                des tombeaux.
                            </span>
                        </p>

                        <p
                            className={`${styles.cadenas} ${
                                sectionCadenas ? "" : styles.animText
                            }`}
                            ref={sectionCadenasText}
                        >
                            Vous trouverez également <br></br>{" "}
                            <span className={styles.fontW2}>des indices</span>
                            <br></br> près du{" "}
                            <span className={styles.fontW1}> cadenas</span>
                        </p>
                    </section>
                    <div
                        className={`${styles.missionPicker} ${
                            isMissionPicker ? "" : styles.missionAppear
                        }`}
                        ref={missionPicker}
                    >
                        <MissionBtn
                            mode="unlocked"
                            id={0}
                            onClick={() => start(0)}
                            active={true}
                        ></MissionBtn>
                        <MissionBtn
                            mode="locked"
                            id={1}
                            active={false}
                        ></MissionBtn>
                        <MissionBtn
                            mode="locked"
                            id={2}
                            active={false}
                        ></MissionBtn>
                    </div>
                </section>
            </GlobalWrapper>
        </div>
    );
};

export default StartingScreen;
