import React from "react";
import Home from "./home/home";

const index = () => (
    <div>
        <Home />
    </div>
);

export default index;
