import React from "react";
import styles from "./toolpres.module.scss";
import Container from "../../comps/container/container";
import Button from "../../comps/button";
import PresTool from "../../comps/pres-tool";
import { useRouter } from "next/router";
import RoomStore from "store/room";

const ToolPres = () => {
    // const router = useRouter();
    // const goToTools = () => {
    //     RoomStore.setGameState("tools-menu");
    //     router.push("/is-tools-menu");
    // };
    return (
        <div>
            <Container fullVH>
                <PresTool
                    tool="Lunette astronomique"
                    scientist="Galilée"
                ></PresTool>
            </Container>
        </div>
    );
};

export default ToolPres;
