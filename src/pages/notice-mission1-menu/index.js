import React from "react";
import { useRouter } from "next/router";
import styles from "./notice-mission1-menu.module.scss";
import Header from "comps/header";
import BackButton from "comps/back-button";
import NoticeHeader from "comps/notice-header";
import GlobalWrapper from "comps/globalWrapper";
import { otherChapters } from "utils/otherChapters";

export const resources = {
    videos: {},
    images: {
        lunette: {
            png: require("./imgs/lunette.png"),
            webp: require("./imgs/lunette.png?webp"),
        },
    },
};

const NoticeMission1Menu = () => {
    const router = useRouter();
    return (
        <GlobalWrapper beige>
            <NoticeHeader
                tool="de la lunette astronomique"
                main={true}
            ></NoticeHeader>
            <div className={styles.noticeContainer}>
                <picture>
                    <source
                        srcSet={resources.images.lunette.webp}
                        type="image/webp"
                    />
                    <source
                        srcSet={resources.images.lunette.png}
                        type="image/png"
                    />
                    <img
                        className={styles.img}
                        src={resources.images.lunette.png}
                    />
                </picture>
                <div
                    className={styles.circleAstre}
                    onClick={() => router.replace("/notice-mission1-astre")}
                >
                    <img src={otherChapters.astre.img} />

                    <p>Les astres</p>
                </div>
                <div
                    className={styles.circleTopRight}
                    onClick={() => router.replace("/notice-mission1-objectif")}
                >
                    <img src={require("./imgs/blue-circle.png")} />
                    <p>L'objectif</p>
                </div>
                <div
                    className={styles.circleleftBottom}
                    onClick={() => router.replace("/notice-mission1-oculaire")}
                >
                    <img src={require("./imgs/blue-circle.png")} />
                    <p>L'oculaire</p>
                </div>
                <div className={styles.test}></div>
            </div>
            <Header>
                <BackButton
                    onClick={() => router.replace(`/is-tools-menu`)}
                ></BackButton>
            </Header>
        </GlobalWrapper>
    );
};

export default NoticeMission1Menu;
