import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Head from "next/head";

import "mobx-react-lite/batchingForReactDom";
import { useObserver } from "mobx-react-lite";

// eslint-disable-next-line no-unused-vars
import styles from "./global.scss";

import localforage from "localforage";
import fscreen from "fscreen";
import { getRoomRoute } from "../utils/route";

import UserStore from "../store/user";
import RoomStore from "../store/room";

import socket from "../comps/socket";

import Loader from "../comps/loader";

import * as Sentry from "@sentry/browser";
import * as SentryIntegrations from "@sentry/integrations";
if (process.env.ENV === "production") {
    Sentry.init({
        dsn: process.env.SENTRY_DSN,
        release: process.env.RELEASE,
        integrations: [
            new SentryIntegrations.CaptureConsole({ levels: ["error"] }),
            new SentryIntegrations.Dedupe(),
            new SentryIntegrations.ExtraErrorData({ depth: 5 }),
        ],
    });
    console.log("Sentry a bien été initialisé");
}

// Prevents memory leaks on SSR
import { useStaticRendering } from "mobx-react-lite";
import { reaction } from "mobx";
import DesktopLanding from "comps/desktopLanding";

if (typeof window === "undefined") {
    useStaticRendering(true);
}
let desktop = false;
if (
    typeof window !== "undefined" &&
    window.matchMedia("(min-width: 600px)").matches
) {
    desktop = true;
}

// Setup fullscreen
if (
    typeof window !== "undefined" &&
    process.env.ENV === "production" &&
    !desktop &&
    fscreen.fullscreenEnabled
) {
    const goFullscreen = () => {
        if (fscreen.fullscreenElement !== null) return;
        const promise = fscreen
            .requestFullscreenFunction(document.documentElement)
            .bind(document.documentElement)({
            navigationUI: "hide",
        });

        if (typeof promise.catch === "function") {
            // console.log(screen.orientation);
            promise.then(() => {
                if (typeof ScreenOrientation.lock === "function")
                    ScreenOrientation.lock("portrait");
                if (typeof screen.orientation.lock === "function")
                    screen.orientation.lock("portrait");
            });
            promise.catch(() => true);
        }
    };
    // document.documentElement.addEventListener("touchstart",
    //     goFullscreen
    // );
    document.documentElement.addEventListener("click", goFullscreen);
}

// Setup localStorage

localforage.config({
    name: "mecanima",
    version: 1.0,
    storeName: "data", // Should be alphanumeric, with underscores.
    description: "Data persisted by the CNAM-game",
});

// Room delete for production room fix
if (typeof window !== "undefined") {
    window.deleteRoom = function (roomId) {
        RoomStore.delete(roomId || RoomStore.id);
    };
}

function App({ Component, pageProps }) {
    const router = useRouter();
    const [loading, setLoading] = useState(true);
    const [ready, setReady] = useState(false);
    const setup = async () => {
        UserStore.setup();
        let roomId = await localforage.getItem("room");
        if (!roomId) {
            if (router.pathname !== "/") {
                router.replace("/");
            }
            setLoading(false);
        } else {
            RoomStore.join(roomId)
                .then(() => {
                    const newRoute = getRoomRoute("connect");
                    if (newRoute === router.pathname) setReady(true);
                    router.push(newRoute).then(() => setReady(true));
                    setLoading(false);
                })
                .catch(() => {
                    console.error("Impossible de rejoindre la salle...");
                    localforage.removeItem("room");
                    router.replace("/");
                    setLoading(false);
                });
        }
    };
    // Setup room on connect and reconnect
    useEffect(() => {
        if (typeof window !== "undefined" && !desktop) {
            setup();
            socket.on("reconnect", setup);
        }
        // else if (typeof window !== "undefined" && desktop) {
        // }
    }, []);
    // Setup route when any state changes
    useEffect(
        () =>
            reaction(
                () => [RoomStore.gameState, RoomStore.roomState],
                () => {
                    console.log("Checking route");

                    // console.log(RoomStore.gameState, RoomStore.roomState);
                    router.push(getRoomRoute("room"));
                    // .then
                    // // () => RoomStore.gameState !== null && setReady(true)
                    // ();
                }
            ),
        []
    );
    useEffect(() => {
        if (
            router.pathname === "/home" ||
            router.pathname === "/room" ||
            router.pathname === "/"
        )
            setReady(true);
        if (desktop && router.pathname !== "/") router.replace("/");
    });
    useEffect(() => {
        function processOrientation(forceOrientation) {
            var orientation = window.orientation;
            if (forceOrientation != undefined) orientation = forceOrientation;
            var domElement = document.documentElement;
            let width;
            let height;
            switch (orientation) {
                case 90:
                    width = window.innerHeight;
                    height = window.innerWidth;
                    domElement.style.width = "100vh";
                    domElement.style.height = "100vw";
                    domElement.style.transformOrigin = "50% 50%";
                    domElement.style.transform =
                        "translate(" +
                        (window.innerWidth / 2 - width / 2) +
                        "px, " +
                        (window.innerHeight / 2 - height / 2) +
                        "px) rotate(-90deg)";
                    break;
                case -90:
                    width = window.innerHeight;
                    height = window.innerWidth;
                    domElement.style.width = "100vh";
                    domElement.style.height = "100vw";
                    domElement.style.transformOrigin = "50% 50%";
                    domElement.style.transform =
                        "translate(" +
                        (window.innerWidth / 2 - width / 2) +
                        "px, " +
                        (window.innerHeight / 2 - height / 2) +
                        "px) rotate(90deg)";
                    break;
                default:
                    domElement.style.width = "100%";
                    domElement.style.height = "100%";
                    domElement.style.transformOrigin = "";
                    domElement.style.transform = "";
                    break;
            }
        }
        if (!desktop) {
            window.addEventListener("orientationchange", processOrientation);
            processOrientation();
            return () => {
                window.removeEventListener(
                    "orientationchange",
                    processOrientation
                );
            };
        }
    });
    return useObserver(() => (
        <>
            <Head>
                <meta
                    name="viewport"
                    content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no,shrink-to-fit=no,viewport-fit=cover"
                ></meta>
                <title>Mecanima</title>
                <meta
                    property="og:title"
                    content="Mecanima - L'escape game du musée des Arts et Métiers"
                />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://mecanima.fr" />
                <meta
                    property="og:image"
                    content="https://mecanima.fr/og.png"
                />
                <meta
                    property="og:description"
                    content="Un escape game innovant basé sur la collaboration, basé sur une intrigue autour du musée des Arts et Métiers, Paris."
                />
                <meta property="og:locale" content="fr_FR" />
                <meta property="og:site_name" content="Mecanima" />

                <script src="/modernizr.js"></script>
            </Head>
            {!desktop && (
                <>
                    {UserStore.loading !== true &&
                    loading !== true &&
                    ready === true ? (
                        <Component {...pageProps} />
                    ) : (
                        <Loader />
                    )}
                </>
            )}
            {desktop && (
                <>
                    {ready === true ? (
                        // <Component {...pageProps} />
                        <DesktopLanding />
                    ) : (
                        <Loader />
                    )}
                </>
            )}
        </>
    ));
}

export default App;
