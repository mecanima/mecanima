import React, { useState } from "react";
import styles from "./is-end.module.scss";
// import { useRouter } from "next/router";
import LastQuestion from "../../comps/last-question";
import HelpBtn from "../../comps/help-btn";
import Header from "../../comps/header";
// import BackButton from "../../comps/back-button";
import Container from "../../comps/container/container";

const IsEnd = () => {
    // const router = useRouter();

    // const [appear, setAppear] = useState(true);

    // const handleAppear = () => {
    // setAppear(false);
    // RoomStore.setPopInOff("first");
    // };

    return (
        <>
            <Container fullHeight>
                <div className={styles.mission1_IntroContainer}>
                    <LastQuestion
                        title="Ne me dites pas que vous pensiez vous échapper aussi facilement ?"
                        text="Une maquette tangible de machine à calculer du 17ème vous aidera à trouver le code de sortie."
                        question="Mais jusqu'à combien cette maquette peut-elle compter ?"
                        destination="end-pres"
                        type="number"
                    ></LastQuestion>
                </div>
            </Container>
            <Header>
                {/* <BackButton onClick={() => router.replace("/is-end-pres")}></BackButton> */}
                <HelpBtn></HelpBtn>
            </Header>
        </>
    );
};

export default IsEnd;
