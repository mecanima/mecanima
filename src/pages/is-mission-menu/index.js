import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { useObserver } from "mobx-react-lite";

import Container from "comps/container/container";
import Header from "comps/header/";
import BackButton from "comps/back-button/";
import MissionBtn from "comps/mission-btn";
import Button from "comps/button";
import GlobalWrapper from "comps/globalWrapper";
import RoomStore from "store/room";

import styles from "./mission-menu.module.scss";

const MissionMenu = () => {
    const router = useRouter();
    // const [lastFound, setLastFound] = useState(-1);
    // const [canEscape, setCanEscape] = useState(false);

    const continueToEnd = () => {
        RoomStore.setGameState("end");
        router.replace("/is-end");
    };

    return useObserver(() => {
        const canEscape = RoomStore.finalAnswers.every(
            (elm) => elm.found === true
        );
        const foundAnswers = RoomStore.finalAnswers.filter(
            (elm) => elm.found === true
        );
        const lastFound = RoomStore.finalAnswers.indexOf(
            foundAnswers[foundAnswers.length - 1]
        );
        return (
            <div>
                <GlobalWrapper blue page>
                    <Container blue>
                        <h1 className={styles.title}>On a trouvé !</h1>
                        {/* si tu cliques sur l'input ça t'envoie direct pour l'instant, normalement après ça t'enverras si et seulement si tu as mis la bonne réponse */}
                        <MissionBtn
                            mode="current"
                            id={0}
                            title="Régler la lunette astronomique"
                            hint="L'indice à trouver se cache dans les montagnes du ciel."
                            // onClick={() => startMission(0)}
                        ></MissionBtn>
                        <MissionBtn
                            mode={lastFound === 0 ? "unlocked" : "locked"}
                            id={1}
                            title="Régler l'horloge marine"
                            hint="Votre indice à trouver est votre longitude en mer."
                        ></MissionBtn>
                        <MissionBtn
                            mode={lastFound === 1 ? "unlocked" : "locked"}
                            id={2}
                            title="Régler la lunette astronomique"
                            hint="Votre indice se cache dans les montagnes du ciel."
                        ></MissionBtn>
                        <Button
                            text="S'échapper !"
                            disabled={!canEscape}
                            onClick={continueToEnd}
                        ></Button>
                    </Container>
                </GlobalWrapper>
                <Header>
                    <BackButton onClick={() => router.back()}></BackButton>
                </Header>
            </div>
        );
    });
};

// const ObserverRoom = observer(Room);
// const RoomComponent = () => <ObserverRoom users={RoomStore.users} />;

export default MissionMenu;
