import React, { useState } from "react";
import styles from "./lenses.module.scss";
import Container from "comps/container/container";
import Header from "comps/header";
import ArrowBtn from "comps/arrow-btn";
import HelpBtn from "comps/help-btn";
import FoundBtn from "comps/found-btn";
import MissionDescription from "comps/mission-description";
import LensesBar from "comps/lenses-bar";
// import LensesBar2 from "comps/lenses-bar-copy";
import GlobalWrapper from "comps/globalWrapper";
import { useObserver } from "mobx-react-lite";
import { useRouter } from "next/router";

import RoomStore from "store/room";
import { debounce } from "lodash";

const sendToolData = debounce(
    (top, bottom) => {
        RoomStore.updateToolData("lenses", [top, bottom]);
    },
    1000,
    { leading: true }
);
const Lenses = () => {
    const router = useRouter();
    const lensesData = RoomStore.tools.find((elm) => elm.name === "lenses");
    const [topLens, setTopLens] = useState(
        (lensesData && lensesData.sentAnswer[0]) || "convergent"
    );
    const [bottomLens, setBottomLens] = useState(
        (lensesData && lensesData.sentAnswer[1]) || "convergent"
    );

    const throwUpTool = () => {
        RoomStore.pickTool("lenses", false);
        router.replace("/is-tools-menu");
    };

    const saveTopLens = (lens) => {
        console.log("Changing top: ", lens);
        setTopLens(lens);
        sendToolData(lens, bottomLens);
    };
    const saveBottomLens = (lens) => {
        console.log("Changing bottom: ", lens);
        setBottomLens(lens);
        sendToolData(topLens, lens);
    };

    return useObserver(() => (
        <>
            <GlobalWrapper beige>
                <MissionDescription
                    missionNum="1"
                    desc="Vous êtes le maître verrier"
                ></MissionDescription>
                <Container fullHeight>
                    <section className={styles.glassContainer}>
                        <LensesBar
                            up={true}
                            callback={saveTopLens}
                            initialStep={lensesData && lensesData.sentAnswer[0]}
                        ></LensesBar>
                        <img
                            className={styles.background_Img}
                            src={require("./imgs/lunette.png")}
                        />
                        <LensesBar
                            up={false}
                            callback={saveBottomLens}
                            initialStep={lensesData && lensesData.sentAnswer[1]}
                        ></LensesBar>
                    </section>
                    <p className={styles.state}>
                        {(topLens === "convergent" ||
                            bottomLens === "divergent") &&
                            !(
                                topLens === "convergent" &&
                                bottomLens === "divergent"
                            ) &&
                            "C'est peut-être une bonne piste, mais c'est encore un peu flou"}
                        {topLens !== "convergent" &&
                            bottomLens !== "divergent" &&
                            "On voit rien avec ça, il faut trouver mieux..."}
                        {topLens === "convergent" &&
                            bottomLens === "divergent" &&
                            "Eh ben voilà, quand vous voulez !"}
                    </p>
                </Container>
            </GlobalWrapper>
            <Header>
                <ArrowBtn onClick={throwUpTool}></ArrowBtn>
                <HelpBtn
                    onClick={() => RoomStore.pickTool("lenses", false)}
                    replace
                ></HelpBtn>
                <FoundBtn
                    beforeClick={() => RoomStore.pickTool("lenses", false)}
                ></FoundBtn>
            </Header>
        </>
    ));
};

// const ObserverRoom = observer(Room);
// const RoomComponent = () => <ObserverRoom users={RoomStore.users} />;

export default Lenses;
