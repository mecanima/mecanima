import React, { useEffect, useRef } from "react";
import styles from "./notice-mission1-oculaire.module.scss";
import NoticeHeader from "../../comps/notice-header";
import OtherChapter from "../../comps/other-chapter";
import Header from "../../comps/header";
import { useRouter } from "next/router";
import ArrowBtn from "../../comps/arrow-btn";
import VideoBackground from "../../comps/videoBackground";
import GlobalWrapper from "../../comps/globalWrapper";
import { useIntersection } from "react-use";

import { otherChapters } from "../../utils/otherChapters";

export const resources = {
    videos: {
        oculaire: require("./imgs/oculaire.mp4"),
        oculaire2: require("./imgs/oculaire2.mp4"),
    },
    images: {},
};

const NoticeMission1Oculaire = () => {
    const router = useRouter();

    let isSecondAppear = false;
    let isChapterAppear = false;
    const secondText = useRef(null);
    const Chapter = useRef(null);
    const video = useRef(null);
    const sectionVideo = useRef(null);

    const secondIntersection = useIntersection(secondText, {
        root: null,
        rootMargin: "0px",
        threshold: 1,
    });
    const videoIntersection = useIntersection(sectionVideo, {
        root: null,
        rootMargin: "0px",
        threshold: 1,
    });
    const chapterIntersection = useIntersection(Chapter, {
        root: null,
        rootMargin: "0px",
        threshold: 0.5,
    });

    if (video.current !== null)
        videoIntersection && videoIntersection.intersectionRatio < 1
            ? video.current.pause()
            : video.current.play();
    secondIntersection && secondIntersection.intersectionRatio < 1
        ? (isSecondAppear = true)
        : null;
    chapterIntersection && chapterIntersection.intersectionRatio < 0.5
        ? null
        : (isChapterAppear = true);

    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    return (
        <GlobalWrapper beige>
            <div className={styles.noticeContainer}>
                <NoticeHeader tool="L'oculaire" main={false}></NoticeHeader>
                <div className={`${styles.sectionContainer} ${styles.mt}`}>
                    <VideoBackground
                        loop={false}
                        playsInline
                        autoPlay
                        url={resources.videos.oculaire}
                        className={styles.oculaire}
                    ></VideoBackground>
                    <div className={styles.textContainer}>
                        <p className={styles.firstAnimText}>
                            Le champs de vision de la lunette de Galilée était
                            très faible,{" "}
                            <span className={styles.fontW1}>d’environ 15°</span>
                            . Il lui était donc impossible de voir toute la Lune
                            d’un coup.
                        </p>
                    </div>
                </div>
                <div
                    className={`${styles.sectionContainer}`}
                    ref={sectionVideo}
                >
                    <VideoBackground
                        loop={false}
                        playsInline
                        autoPlay={false}
                        ref={video}
                        url={resources.videos.oculaire2}
                        className={styles.oculaire}
                    ></VideoBackground>
                    <div className={styles.textContainer}>
                        <p
                            ref={secondText}
                            className={isSecondAppear ? "" : styles.animText}
                        >
                            Il n’en voyait que de petits bouts à la fois...{" "}
                            <span className={styles.fontW1}>
                                pas de vision globale
                            </span>
                            . Il la reconstituait à partir d’observations de{" "}
                            <span className={styles.fontW1}>
                                petites zones juxtaposées
                            </span>
                            .
                        </p>
                    </div>
                </div>
                <div ref={Chapter}>
                    <OtherChapter
                        chapter1={otherChapters.objectif}
                        chapter2={otherChapters.astre}
                        appear={isChapterAppear}
                    ></OtherChapter>
                </div>
                <Header left>
                    <ArrowBtn
                        onClick={() => {
                            router.replace("/notice-mission1-menu");
                        }}
                    ></ArrowBtn>
                </Header>
            </div>
        </GlobalWrapper>
    );
};

export default NoticeMission1Oculaire;
