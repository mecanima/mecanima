import React, { useState } from "react";
import Tools from "../../comps/tools";
import Header from "../../comps/header";
import HelpBtn from "../../comps/help-btn";
import NoticeBtn from "../../comps/notice-btn";
import FoundBtn from "../../comps/found-btn";
import BackButton from "../../comps/back-button";
import MissionDescription from "../../comps/mission-description";
import GlobalWrapper from "../../comps/globalWrapper";
import styles from "./is-tools-menu.module.scss";
import { useRouter } from "next/router";

import RoomStore from "../../store/room/index";
import Head from "next/head";

import { resources as mission1AstreResources } from "pages/notice-mission1-astre";
import { resources as mission1MenuResources } from "pages/notice-mission1-menu";
import { resources as mission1ObjectifResources } from "pages/notice-mission1-objectif";
import { resources as mission1OculaireResources } from "pages/notice-mission1-oculaire";

const videosToPreload = {
    ...mission1AstreResources.videos,
    ...mission1MenuResources.videos,
    ...mission1ObjectifResources.videos,
    ...mission1OculaireResources.videos,
};
const imagesToPreload = {
    ...mission1AstreResources.images,
    ...mission1MenuResources.images,
    ...mission1ObjectifResources.images,
    ...mission1OculaireResources.images,
};

export default function ToolsMenu() {
    const router = useRouter();
    const [appear, setAppear] = useState(true);
    let imgToUse;
    if (typeof window !== "undefined" && window.Modernizr)
        // eslint-disable-next-line no-undef
        imgToUse = Modernizr.webp ? "webp" : "png";
    const handleAppear = () => {
        setAppear(false);
        RoomStore.setPopInOff("second");
    };
    return (
        <>
            <Head>
                {Object.values(videosToPreload).map((video) => (
                    <link
                        key={video}
                        rel="prefetch"
                        as="video"
                        href={video}
                    ></link>
                ))}
                {Object.values(imagesToPreload).map((image) => (
                    <link
                        key={image.png}
                        rel="prefetch"
                        as="image"
                        href={imgToUse ? image[imgToUse] : image.png}
                    ></link>
                ))}
                {/* <link
                    rel="prefetch"
                    as="image"
                    href={require("pages/notice-mission1-astre/imgs/astre.png")}
                ></link>
                <link
                    rel="prefetch"
                    as="image"
                    href={require("pages/notice-mission1-astre/imgs/astre2.png")}
                ></link>
                <link
                    rel="prefetch"
                    as="image"
                    href={require("pages/notice-mission1-astre/imgs/livre.png")}
                ></link> */}
            </Head>
            <section
                className={`${styles.pop_in} ${
                    appear && !RoomStore.secondPopIn ? "" : styles.appear
                }`}
                onClick={handleAppear}
            >
                <div className={styles.inside__Block}>
                    <picture>
                        <source
                            srcSet={require("./background.png?webp")}
                            type="image/webp"
                        />
                        <source
                            srcSet={require("./background.png")}
                            type="image/png"
                        />
                        <img
                            className={styles.background_Img}
                            src={require("./background.png")}
                        />
                    </picture>
                    <h4>
                        Une série d’outils virtuels <br></br> s’offre à vous.
                    </h4>
                    <p>
                        Chaque membre de l’équipe devra simultanément effectuer
                        un réglage sur un outil. <br></br>
                        Les actions de chacun auront des conséquences sur le
                        fonctionnement de la lunette astronomique.
                    </p>
                    <BackButton onClick={() => setAppear(false)}></BackButton>
                </div>
            </section>
            <GlobalWrapper beige>
                <MissionDescription
                    missionNum="1"
                    title="Régler la lunette astronomique"
                    desc="L'indice à trouver se cache dans les montagnes du ciel."
                ></MissionDescription>
                <Tools></Tools>
            </GlobalWrapper>
            <Header>
                <NoticeBtn
                    onClick={() => router.push("/notice-mission1-menu")}
                ></NoticeBtn>
                <HelpBtn></HelpBtn>
                <FoundBtn></FoundBtn>
            </Header>
        </>
    );
}
