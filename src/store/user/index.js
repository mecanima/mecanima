import socket from "../../comps/socket";
import localforage from "localforage";
import { v4 as uuidv4 } from "uuid";

import { observable, action } from "mobx";

class User {
    @observable id = "";
    @observable loading = true;
    @observable reconnecting = false;
    @action async setup() {
        const userId = await localforage.getItem("userId");
        if (!userId) {
            this.id = uuidv4();
            localforage.setItem("userId", this.id);
        } else {
            this.id = userId;
        }
        socket.emit("setup", this.id, () => (this.loading = false));
        socket.on("reconnecting", () => (this.reconnecting = true));
        socket.on(
            "reconnect",
            () => (this.reconnecting = false)
            // socket.emit("setup", this.id, () => (this.reconnecting = false))
        );
    }
}

export default new User();
