import socket from "../../comps/socket";
import { roomHandler, gameHandler } from "./handler";

import { observable, action } from "mobx";
import { computedFn } from "mobx-utils";

class Room {
    @observable id = "";
    @observable gameName = "";
    @observable users = [];
    @observable firstPopIn = false;
    @observable secondPopIn = false;
    @observable thirdPopIn = false;
    @observable roomState = null;
    @observable gameState = null;
    @observable entryAnswer = {
        length: 0,
        text: "",
        result: false,
    };
    @observable finalAnswers = [];
    @observable finalChallenge = [];
    @observable tools = [];
    @observable hintsData = {};
    @action join(id) {
        return new Promise((resolve, reject) => {
            socket.emit(
                "room",
                { action: "join", payload: { roomId: id } },
                (res) => {
                    if (res === false) {
                        reject(res);
                        return;
                    }
                    this.users = res.users;
                    this.id = id;
                    this.gameName = res.gameName;
                    this.roomState = res.state;
                    if (typeof res.game.state !== "undefined")
                        this.gameState = res.game.state;
                    if (
                        res.state !== null &&
                        typeof res.game.currentMission !== "undefined"
                    )
                        this.setStepData(res.game);
                    this.setupListeners();
                    resolve();
                }
            );
        });
    }
    @action setupListeners() {
        socket.on("room", roomHandler.bind(this));
        socket.on("game", gameHandler.bind(this));
    }
    @action removeUser(user) {
        const localUser = this.users.findIndex((elm) => elm.id === user.id);
        if (localUser >= 0) this.users.splice(localUser, 1);
    }
    @action pickTool(tool, side) {
        return new Promise((resolve, reject) => {
            socket.emit(
                "game",
                {
                    action: "toolPickup",
                    payload: {
                        tool,
                        side,
                    },
                },
                (res) => {
                    if (res === false) reject();
                    resolve(res);
                }
            );
        });
    }
    @action updateToolData(tool, data) {
        socket.emit("game", {
            action: "updateTool",
            payload: {
                tool,
                data,
            },
        });
        const toolObj = this.tools.find((elm) => elm.name === tool);
        toolObj.sentAnswer = data;
    }
    @action revealHint(index) {
        const hint = this.hintsData.hints[index];
        if (this.hintsData.hintsPoints >= hint.cost) {
            socket.emit(
                "game",
                {
                    action: "revealHint",
                    payload: {
                        index,
                    },
                },
                (res) => {
                    if (res !== false) {
                        this.hintsData = res;
                    }
                }
            );
        }
    }
    @action startRoom() {
        socket.emit("room", { action: "start" });
    }
    @action setGameState(state, mission) {
        socket.emit("game", { action: "state", payload: { mission, state } });
        if (this.gameState !== state) this.gameState = state;
    }

    @action setHintsCollection(step) {
        socket.emit(
            "game",
            {
                action: "setHintsCollection",
                payload: { name: step },
            },
            (res) => {
                if (res !== false) {
                    this.hintsData = res;
                }
            }
        );
    }

    @action sendEntryText(text) {
        this.entryAnswer.text = text;
        socket.emit(
            "game",
            {
                action: "entryAnswer",
                payload: { text: this.entryAnswer.text },
            },
            (data) =>
                (this.entryAnswer = { ...this.entryAnswer, ...data.payload })
        );
    }

    @action setFinalAnswer(id, text) {
        return new Promise((resolve, reject) => {
            this.finalAnswers[id].value = text;
            socket.emit(
                "game",
                {
                    action: "finalAnswer",
                    payload: { id, text },
                },
                (data) => {
                    this.finalAnswers[id].found = data.payload.found;
                    resolve(data.payload.found);
                }
            );
        });
    }
    @action setFinalChallenge(id, text) {
        return new Promise((resolve, reject) => {
            this.finalChallenge[id].value = text;
            socket.emit(
                "game",
                {
                    action: "finalChallenge",
                    payload: { id, text },
                },
                (data) => {
                    this.finalChallenge[id].found = data.payload.found;
                    resolve(data.payload.found);
                }
            );
        });
    }

    @action setStepData(payload) {
        this.tools = payload.currentMission.tools;
        this.entryAnswer = payload.currentMission.entryAnswer;
        this.finalAnswers = payload.stepsFinalAnswers;
        this.finalChallenge = payload.finalChallenge;
        this.gameState = payload.state;
        this.hintsData = payload.currentMission.hintsData;
    }

    @action setPopInOff(pop) {
        if(pop === "first") {
            this.firstPopIn = true
        } else if(pop === "second") {
            this.secondPopIn = true
        } else if(pop === "third") {
            this.thirdPopIn = true
        }
    }

    getOtherUsers = computedFn(function getOtherUsers(userId) {
        return this.users.filter((user) => user.id !== userId);
    }, true);
    getUser = computedFn(function getUser(userId) {
        return this.users.find((user) => user.id === userId);
    }, true);

    getOtherNames = computedFn(function getOtherNames() {
        const names = [
            "Galilée",
            "Ferdinand Berthoud",
            "Eugène Bourdon",
            "Wilhelm Röntgen",
        ];
        let usersName = [];
        this.users.map((elm) => {
            usersName.push(elm.character.name);
        });

        for (let i = 0; i < usersName.length; i++) {
            if (names.indexOf(usersName[i]) !== -1)
                names.splice(names.indexOf(usersName[i]), 1);
        }
        return names;
    });

    getUserPosition = computedFn(function getUserPosition(id) {
        return this.users.findIndex((elm) => elm.id === id);
    });

    delete = (roomId) => {
        socket.emit("room", { action: "delete", payload: { roomId } });
        window.location.reload();
    };
}

export default new Room();
