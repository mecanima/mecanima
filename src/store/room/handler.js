export function roomHandler(data) {
    switch (data.action) {
        case "join":
            this.users.push(data.payload.user);
            break;
        case "disconnect":
        case "quit":
            this.removeUser(data.payload.user);
            break;
        case "start":
            this.roomState = "started";
            break;
        default:
            console.error(
                "Action de salle reçue inconnue : ",
                data.action,
                data
            );
    }
}

export function gameHandler(data) {
    switch (data.action) {
        case "state":
            this.gameState = data.payload.state;
            if (data.payload.hintsData) this.hintsData = data.payload.hintsData;
            break;
        case "step":
            this.setStepData(data.payload.game);
            break;
        case "tools":
            if (this.tools.length === data.payload.length) {
                this.tools = this.tools.map(
                    (elm, index) => data.payload[index]
                );
            } else {
                this.tools = data.payload;
            }
            break;
        case "hints":
            this.hintsData = { ...data.payload };
            break;
        case "hintsPoint":
            this.hintsData.hintsPoints = data.payload.points;
            break;
        case "entryAnswer":
            this.entryAnswer = { ...this.entryAnswer, ...data.payload };
            break;
        case "finalAnswers":
            this.finalAnswers = [...data.payload];
            break;
        case "finalChallenge":
            this.finalChallenge = [...data.payload];
            break;
        default:
            console.error("Action de jeu reçue inconnue : ", data.action, data);
    }
}
