import React from "react";
import styles from "./notice-section.module.scss";

const NoticeSection = props => {
    //Pas encore réfléchi à comment on pouvait faire un composant sachant que chaque texte est différents et certains mots ont des taille différentes AHH
    return (
        <div className={styles.sectionContainer}>
            <picture>
                <source
                    srcSet={props.img}
                    type="image/webp"
                />
                <source
                    srcSet={props.img}
                    type="image/png"
                />
                <img
                    className={styles.img}
                    src={props.img}
                />
            </picture>
            <p>{props.text}</p>
        </div>
    );
};

export default NoticeSection;
