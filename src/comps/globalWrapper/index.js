import React from "react";
import styles from "./globalWrapper.module.scss";

const GlobalWrapper = props => {

    return (
       <section className={`${styles.globalWrapper} ${props.blue ? styles.blue : ""} ${props.beige ? styles.beige : ""} ${props.brown ? styles.brown : ""} ${props.page ? styles.page : ""}`}>
           {props.children}
       </section>
    );
};

export default GlobalWrapper;
