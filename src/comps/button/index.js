import React from "react";
import styles from "./button.module.scss";

const Button = props => {
    return (
        <button
            className={`${styles.button_home} ${props.abs ? styles.abs : ""} ${props.disabled ? styles.disabled : ""} ${props.scenario ? styles.scenario : ""}`}
            onClick={props.onClick}
            disabled={props.disabled ? true : false}
        >
            <picture>
                <source srcSet={require("./cta.png?webp")} type="image/webp" />
                <source srcSet={require("./cta.png")} type="image/png" />
                <img src={require("./cta.png")} />
            </picture>
            {/* <img src={require("./cta.png?resize&size=400").src} /> */}
            <span>{props.text}</span>
        </button>
    );
};

export default Button;
