import React from "react";
import styles from "./backButton.module.scss";

const BackButton = props => {
    return (
       <button className={styles.backButton} onClick={props.onClick}>
           <img src={require("./picto-croix.png")} />
       </button>
    );
};

export default BackButton;
