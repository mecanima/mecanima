import React from "react";
import styles from "./tool.module.scss";
import { observer } from "mobx-react-lite";

import RoomStore from "../../store/room";

const tools = {
    telescope: {
        id: 4,
        name: "L'objectif",
        job: "L'astronome",
        img: {
            src: require("./imgs/telescope.png?resize&size=300"),
            webp: require("./imgs/telescope.png?webp"),
        },
        ajustment: true,
    },
    calendar: {
        id: 2,
        name: "Le calendrier lunaire",
        job: "Maître du temps",
        img: {
            src: require("./imgs/lunarcalendar.png?resize&size=300"),
            webp: require("./imgs/lunarcalendar.png?webp"),
        },
        ajustment: true,
    },
    compass: {
        id: 1,
        name: "La boussole",
        job: "Maître de l'orientation",
        img: {
            src: require("./imgs/compass.png?resize&size=300"),
            webp: require("./imgs/compass.png?webp"),
        },
        ajustment: false,
    },
    lenses: {
        id: 3,
        name: "Les lentilles",
        job: "Maître verrier",
        img: {
            src: require("./imgs/lenses.png?resize&size=300"),
            webp: require("./imgs/lenses.png?webp"),
        },
        ajustment: false,
    },
};

const Tool = observer((props) => {
    // const [isUsed, setIsUsed] = useState(false);
    const outil = tools[props.tool];
    const proxy = RoomStore.tools.find((elm) => elm.name === props.tool);
    if (typeof outil === "undefined")
        return <p>L'outil n'est pas encore configuré, on dirait !</p>;
    return (
        <div onClick={props.onClick}>
            <div
                className={`${styles.toolText} ${
                    props.selected !== outil.id ? styles.noText : ""
                }`}
            >
                <h4>{outil.name}</h4>
                <p>{outil.job}</p>
            </div>
            <aside
                className={`${styles.used} ${
                    proxy.pickedBy !== null ? styles.open : ""
                }`}
            >
                en cours d'utilisation
            </aside>
            <div className={styles.imagesContainer}>
                <img
                    className={styles.background_Img}
                    src={require("./background.png")}
                />
                <picture>
                    <source srcSet={outil.img.webp} type="image/webp" />
                    <source srcSet={outil.img.src} type="image/png" />
                    <img
                        // className={styles.tool_Img}
                        className={`${styles.tool_Img} ${
                            proxy.pickedBy !== null ? styles.grey : ""
                        } ${outil.ajustment ? styles.ajustment : ""}`}
                        style={{ animationDelay: props.animationDelay }}
                        src={outil.img.src}
                        alt={outil.name}
                    />
                </picture>
            </div>
            {/* <img
                    className={`${styles.tool_Img} ${isUsed ? styles.grey : ""}`}
                    alt=""
                    src={props.image}
                ></img> */}
            <button className={styles.button__use}>Utiliser</button>
        </div>
    );
});

export default Tool;
