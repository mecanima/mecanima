import React from "react";
import { useRouter } from "next/router";

import RoomStore from "store/room";

import styles from "./found-btn.module.scss";

const FoundBtn = ({ onClick, beforeClick }) => {
    const router = useRouter();
    return (
        <button
            className={styles.foundBtn}
            onClick={(e) => {
                if (onClick) {
                    onClick(e);
                } else {
                    if (typeof beforeClick === "function") beforeClick(e);
                    router.replace(`/${RoomStore.gameName}-mission-menu`);
                }
            }}
        >
            <img src={require("./cle.png")} />
            On a trouvé !
        </button>
    );
};

export default FoundBtn;
