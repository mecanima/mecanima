import React from "react";
import styles from "./pres-tool.module.scss";
import Container from "../container/container";
import Button from "../button/";
import { useRouter } from "next/router";
import RoomStore from "store/room";

const PresTool = (props) => {
    const router = useRouter();
    const goToTools = () => {
        RoomStore.setGameState("tools-menu");
        router.push("/is-tools-menu");
    };
    return (
        <>
            {/* <Container fullHeight> */}
            <div className={styles.presContainer}>
                <picture>
                    <source
                        srcSet={require("./background.png?webp")}
                        type="image/webp"
                    />
                    <source
                        srcSet={require("./background.png")}
                        type="image/png"
                    />
                    <img
                        className={styles.img}
                        src={require("./background.png")}
                    />
                </picture>
                <picture>
                    <source
                        srcSet={require("./lunette.png?webp")}
                        type="image/webp"
                    />
                    <source
                        srcSet={require("./lunette.png")}
                        type="image/png"
                    />
                    <img
                        className={styles.img}
                        src={require("./lunette.png")}
                    />
                </picture>
            </div>
            <div className={styles.presDescription}>
                <h4 className={styles.title}>
                    <p className={styles.title_bold}>{props.tool}</p>
                    <p className={styles.scientist}>{props.scientist}</p>
                </h4>
                <p className={styles.text}>
                    Votre 1ère mission est d’utiliser cet objet. Comme dans
                    toute équipe de chercheurs, la coopération sera le maître
                    mot.{" "}
                </p>
            </div>
            <Button
                text="C'est parti !"
                // abs={true}
                onClick={goToTools}
            ></Button>
            {/* </Container> */}
        </>
    );
};

export default PresTool;
