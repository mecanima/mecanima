import React from "react";
import styles from "./other-chapter.module.scss";
import { useRouter } from "next/router";

const OtherChapter = props => {
    const router = useRouter();
    return (
        <section className={styles.otherChapters}>
            <h4 
                className={props.appear ? styles.textAppear : ""}
                >
                Découvrir<br></br> les autres chapitres</h4>
            <div className={`${styles.buttonsContainer} ${props.appear ? styles.buttonAppear : ""}`}>
                <div className={styles.buttonContainer}>
                    <button onClick={() => router.replace(`/notice-${props.chapter1.route}`)}>
                    <img
                        src={props.chapter1.img}
                    />
                    </button>
                    <p>{props.chapter1.title}</p>
                </div>
                <div 
                    className={styles.buttonContainer} 
                    >
                    <button onClick={() => router.replace(`/notice-${props.chapter2.route}`)}>
                    <img
                        src={props.chapter2.img}
                    />
                    </button>
                    <p>{props.chapter2.title}</p>
                </div>
            </div>
        </section>
    );
};

export default OtherChapter;
