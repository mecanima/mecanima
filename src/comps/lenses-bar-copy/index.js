import React, { useState, useRef } from "react";
import styles from "./lenses-bar.module.scss";
import RoomStore from "../../store/room";
import Flickity from "react-flickity-component";
import Lenses from "../lenses";
import { lensesTab } from "../../utils/lenses";

const LensesBar2 = (props) => {
    const lenses = {
        convergente_plat: {
            step: "convergente_plat",
            img: {
                src: require("./imgs/convergente-plat.png"),
                webp: require("./imgs/convergente-plat.png?webp"),
            },
        },
        convergent: {
            step: "convergent",
            img: {
                src: require("./imgs/convergent.png"),
                webp: require("./imgs/convergent.png?webp"),
            },
        },
        divergent: {
            step: "divergent",
            img: {
                src: require("./imgs/divergent.png"),
                webp: require("./imgs/divergent.png?webp"),
            },
        },
    };

    const stepsArray = Object.keys(lenses);

    let leftStepIndex =
        stepsArray.findIndex((elm) => elm === props.initialStep) - 1;
    if (leftStepIndex < 0) leftStepIndex = stepsArray.length - 1;
    let rightStepIndex =
        stepsArray.findIndex((elm) => elm === props.initialStep) + 1;
    if (rightStepIndex >= stepsArray.length) rightStepIndex = 0;

    // const [leftLense, setLeftLense] = useState(leftStep.img.src);
    // const [middleLense, setMiddleLense] = useState(
    //     lenses[props.initialStep].img.src
    // );
    // const [rightLense, setRightLense] = useState(rightStep.img.src);
    const [leftLenseStep, setLeftLenseStep] = useState(
        lenses[stepsArray[leftStepIndex] || 0].step
    );
    const [middleLenseStep, setMiddleLenseStep] = useState(
        lenses[props.initialStep || "convergent"].step
    );
    const [rightLenseStep, setRightLenseStep] = useState(
        lenses[stepsArray[rightStepIndex] || 2].step
    );

    const changeLense = (lensePlace) => {
        let newState;
        if (lensePlace === "left") {
            newState = leftLenseStep;
            // setMiddleLense(leftLense);
            // setLeftLense(middleLense);
            setMiddleLenseStep(leftLenseStep);
            setLeftLenseStep(middleLenseStep);
        } else {
            newState = rightLenseStep;
            // setMiddleLense(rightLense);
            // setRightLense(middleLense);
            setMiddleLenseStep(rightLenseStep);
            setRightLenseStep(middleLenseStep);
        }
        if (typeof props.callback === "function") props.callback(newState);
        //La valeur que l'on veut avoir: la lentille du milieu
        //attention, il y a l'info de la barre blanche du bas et celle du haut
    };

    const flickityOptions = {
        wrapAround: true,
        prevNextButtons: false,
        pageDots: false,
        initialIndex: 4,
        selectedAttraction: 0.2,
        friction: 0.8,
    };

    const setupFlickity = (ref) => {
        flickity.current = ref;
        flickity.current.on("change", (index) => {
            setCurrentSlide(index);
        });
    };

    const flickity = useRef(null);
    const [currentSlide, setCurrentSlide] = useState(
        flickityOptions.initialIndex
    );

    return (
        <>
            <style global jsx>{`
                .is-selected {
                    transform: scale(1.2);
                    opacity: 1;
                }
            `}</style>
            <div className={styles.carouselWrapper}>
                <Flickity
                    className={styles.carousel}
                    options={flickityOptions}
                    flickityRef={setupFlickity}
                    static
                    disableImagesLoaded
                >
                    {lensesTab.map((elm, index) => (
                        <div key={index} className={styles.slide}>
                           <Lenses imgUrl={elm.img.src}></Lenses>
                        </div>
                    ))}
                </Flickity>
            </div>
        </>
    );
};

export default LensesBar2;
