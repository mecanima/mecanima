import React, { useState } from "react";
import styles from "./hint.module.scss";
import RoomStore from "store/room";
// import VideoBackground from "../videoBackground";

const enveloppeImage = {
    closed: require("./enveloppe-fermee.png"),
    opened: require("./enveloppe.png"),
    // animated: {
    // webp: "/anims/enveloppe.webp",
    // gif: require("./enveloppe.gif"),
    // },
};

const Hint = (props) => {
    // const [hint, setHint] = useState(false);
    // let launch = false;
    // const [enveloppe, setEnveloppe] = useState(false);

    const handleLaunch = () => {
        // setEnveloppe(true);
        RoomStore.revealHint(props.index);
    };

    return (
        <>
            <button className={`${styles.button_hint}`} onClick={handleLaunch}>
                <div
                    className={`${styles.button} ${
                        props.revealed ? styles.open : ""
                    } ${props.active ? styles.active : ""}`}
                >
                    <img
                        className={styles.background_Img}
                        src={
                            props.revealed
                                ? enveloppeImage.opened
                                : enveloppeImage.closed
                        }
                    />
                    <span className={props.revealed ? styles.hide : ""}>
                        <span className={`${styles.text__bold}`}>coûte</span>{" "}
                        {props.point > 1 ? `${props.point} points` : "1 point"}
                    </span>
                </div>

                <p
                    className={`${styles.hint} ${
                        props.revealed ? styles.animText : ""
                    }`}
                >
                    {props.hint}
                </p>
            </button>
        </>
    );
};

export default Hint;
