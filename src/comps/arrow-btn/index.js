import React from "react";
import styles from "./arrow-btn.module.scss";

const ArrowBtn = props => {
    return (
       <button className={styles.arrowButton} onClick={props.onClick}>
           <img src={require("./retour.png")} />
       </button>
    );
};

export default ArrowBtn;
