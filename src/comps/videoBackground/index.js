import React, { forwardRef } from "react";
// import styles from "./videoBackground.module.scss";

const VideoBackground = (props, ref) => {
    return (
        <video
            ref={ref}
            poster={props.poster}
            controls={
                typeof props.controls !== "undefined" ? props.controls : false
            }
            muted={typeof props.muted !== "undefined" ? props.muted : true}
            loop={props.loop}
            playsInline={props.playsInline}
            className={`${props.className ? props.className : ""}`}
            autoPlay={props.autoPlay}
        >
            <source src={props.url} type="video/mp4" />
        </video>
    );
};

export default forwardRef(VideoBackground);
