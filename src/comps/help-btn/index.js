import React from "react";
import styles from "./help-btn.module.scss";
import { useRouter } from "next/router";

const HelpBtn = ({ onClick, replace }) => {
    const router = useRouter();
    const clickHandler = (evt) => {
        if (typeof onClick === "function") onClick(evt);
        if (replace === true) {
            router.replace("/help");
        } else {
            router.push("/help");
        }
    };
    return (
        <button className={styles.helpBtn} onClick={clickHandler}>
            {/* Not needed, the PNG is small enough to be inlined */}
            {/* <picture>
                <source
                    srcSet={require("./indices.png?webp")}
                    type="image/webp"
                />
                <source srcSet={require("./indices.png")} type="image/png" />
                <img src={require("./indices.png")} />
            </picture> */}
            <img src={require("./indices.png")} />
            Indices
        </button>
    );
};

export default HelpBtn;
