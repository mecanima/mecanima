import React, { useState } from "react";
import styles from "./lenses-bar.module.scss";
import RoomStore from "../../store/room";

const LensesBar = (props) => {
    const lenses = {
        convergente_plat: {
            step: "convergente_plat",
            img: {
                src: require("./imgs/convergente-plat.png"),
                webp: require("./imgs/convergente-plat.png?webp"),
            },
        },
        convergent: {
            step: "convergent",
            img: {
                src: require("./imgs/convergent.png"),
                webp: require("./imgs/convergent.png?webp"),
            },
        },
        divergent: {
            step: "divergent",
            img: {
                src: require("./imgs/divergent.png"),
                webp: require("./imgs/divergent.png?webp"),
            },
        },
    };

    const stepsArray = Object.keys(lenses);

    let leftStepIndex =
        stepsArray.findIndex((elm) => elm === props.initialStep) - 1;
    if (leftStepIndex < 0) leftStepIndex = stepsArray.length - 1;
    let rightStepIndex =
        stepsArray.findIndex((elm) => elm === props.initialStep) + 1;
    if (rightStepIndex >= stepsArray.length) rightStepIndex = 0;

    // const [leftLense, setLeftLense] = useState(leftStep.img.src);
    // const [middleLense, setMiddleLense] = useState(
    //     lenses[props.initialStep].img.src
    // );
    // const [rightLense, setRightLense] = useState(rightStep.img.src);
    const [leftLenseStep, setLeftLenseStep] = useState(
        lenses[stepsArray[leftStepIndex] || 0].step
    );
    const [middleLenseStep, setMiddleLenseStep] = useState(
        lenses[props.initialStep || "convergent"].step
    );
    const [rightLenseStep, setRightLenseStep] = useState(
        lenses[stepsArray[rightStepIndex] || 2].step
    );

    const changeLense = (lensePlace) => {
        let newState;
        if (lensePlace === "left") {
            newState = leftLenseStep;
            // setMiddleLense(leftLense);
            // setLeftLense(middleLense);
            setMiddleLenseStep(leftLenseStep);
            setLeftLenseStep(middleLenseStep);
        } else {
            newState = rightLenseStep;
            // setMiddleLense(rightLense);
            // setRightLense(middleLense);
            setMiddleLenseStep(rightLenseStep);
            setRightLenseStep(middleLenseStep);
        }
        if (typeof props.callback === "function") props.callback(newState);
        //La valeur que l'on veut avoir: la lentille du milieu
        //attention, il y a l'info de la barre blanche du bas et celle du haut
    };

    return (
        <div
            className={`${styles.lensesContainer} ${
                props.up ? styles.up : styles.down
            }`}
        >
            <div
                className={`${styles.lenses}`}
                onClick={() => changeLense("left")}
            >
                <img src={lenses[leftLenseStep].img.src} />
            </div>
            <div className={`${styles.lenses} ${styles.active}`}>
                <img src={lenses[middleLenseStep].img.src} />
            </div>
            <div
                className={`${styles.lenses}`}
                onClick={() => changeLense("right")}
            >
                <img src={lenses[rightLenseStep].img.src} />
            </div>
        </div>
    );
};

export default LensesBar;
