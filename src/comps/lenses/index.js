import React from "react";
import styles from "./lenses.module.scss";

const Lenses = props => {
    return (
       <>
        <img src={props.imgUrl} className={styles.img} />
       </>
    );
};

export default Lenses;
