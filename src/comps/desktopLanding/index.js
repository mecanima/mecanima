import React from "react";
import styles from "./desktop.module.scss";

export default function DesktopLanding() {
    return (
        <>
            <section className={styles.landingSection}>
                <img src={require('./imgs/logo.png')} className={styles.landingSection__img} />
            </section>

            <section className={styles.brefSection}>
                <h1 className={styles.brefSection__title}>Le projet en bref</h1>
                <div className={styles.brefSection__container}>
                    <div className={styles.brefSection__item}>
                        <h4 className={styles.brefSection__item_title}>Quoi</h4>
                        <img className={styles.brefSection__item_img} src={require('./imgs/bref/cadenas.png')} />
                        <p className={styles.brefSection__item_text}>Un escape game <br></br> collaboratif</p>
                    </div>
                    <div className={styles.brefSection__item}>
                        <h4 className={styles.brefSection__item_title}>Où</h4>
                        <img className={styles.brefSection__item_img__lunette} src={require('./imgs/bref/lunette.png')} />
                        <p className={styles.brefSection__item_text}>
                            Au musée des arts et métiers <br></br>
                            <span className={`${styles.brefSection__item_text___tiny}`}>Dans la collection des instruments scientfiques </span>
                        </p>
                    </div>
                    <div className={styles.brefSection__item}>
                        <h4 className={styles.brefSection__item_title}>Quand</h4>
                        <img className={styles.brefSection__item_img} src={require('./imgs/bref/lune.png')} />
                        <p className={styles.brefSection__item_text}>
                            Durant les nocturnes <br></br><span className={`${styles.brefSection__item_text___tiny}`}>Tous les vendredis </span>
                        </p>
                    </div>
                    <div className={styles.brefSection__item}>
                        <h4 className={styles.brefSection__item_title}>Comment</h4>
                        <img className={styles.brefSection__item_img} src={require('./imgs/bref/comment.png')} />
                        <p className={styles.brefSection__item_text}>
                            Bring your own device <br></br>
                            <span className={`${styles.brefSection__item_text___tiny}`}>Les joueurs devront se munir <br></br> de leur propre téléphone.</span>
                        </p>
                    </div>
                    <div className={styles.brefSection__item}>
                        <h4 className={styles.brefSection__item_title}>Pour qui</h4>
                        <img className={styles.brefSection__item_img} src={require('./imgs/bref/players.png')} />
                    </div>
                    <div className={styles.brefSection__item}>
                        <h4 className={styles.brefSection__item_title}>Nombre de joueurs</h4>
                        <img className={styles.brefSection__item_img} src={require('./imgs/bref/age.png')} />
                    </div>
                </div>
            </section>

            <section className={styles.escapeSection}>
                <h1 className={styles.escapeSection__title}>Un escape game <br></br> au musée des arts et métiers</h1>
                <div className={styles.escapeSection__container}>
                    <div className={styles.escapeSection__paragraphe}>
                        <h4 className={styles.escapeSection___title}>Un scénario</h4>
                        <p className={styles.escapeSection__text}>
                            <span className={styles.escapeSection__text___big}>4 chercheurs</span>, venus sur place pour 
                            observer <span className={styles.escapeSection__text___big}>la galerie des instruments scientifiques </span> se retrouvent
                            pris aux  pièges par les <span className={styles.escapeSection__text___big}>esprits de l’église Saint-Martin-des-Champs</span>.
                            Arriveront-ils à s’extirper ? 
                        </p>
                        <article className={styles.escapeSection__relatedContent}>
                            <p className={styles.escapeSection__relatedContent___text}>
                                <span className={styles.escapeSection__relatedContent___text____big}>Les 4 joueurs </span>  
                                commencent l’expérience avec leur propre téléphone comme support de jeu. 
                            </p>
                            <p className={styles.escapeSection__relatedContent___text}>
                                <span className={styles.escapeSection__relatedContent___text____big}>3 missions </span> 
                                les attendent <br></br> avec à la clef une découverte !
                            </p>
                        </article>
                    </div>
                    <img className={styles.escapeSection__img} src={require('./imgs/chercheurs.png')} />
                </div>
            </section>

            <section className={styles.planSection}>
                <div className={styles.planSection__container}>
                    <img className={styles.planSection__img} src={require('./imgs/plan.png')} />
                    <div className={styles.planSection__paragraphe}>
                        <h4 className={styles.planSection__title}>Une visite dynamique</h4>
                        <p className={styles.planSection__text}>
                            Cette expérience créée un parcours qui <span className={styles.planSection__text___tiny}>rompt avec la linéarité</span> d’une visite traditionnelle.
                        </p>
                    </div>
                </div>
            </section>

            <section className={styles.experienceSection}>
                <h1 className={styles.experienceSection__title}>Une expérience collaborative</h1>
                <div className={styles.experienceSection__container}>
                    <div className={styles.experienceSection__paragraphe}>
                        <h4 className={styles.experienceSection___title}>Redonner vie virtuellement aux objets</h4>
                        <p className={styles.experienceSection__text}>
                            Chaque mission se concentre autour <span className={styles.experienceSection__text___big}>d’un objet</span> de la collection des 
                            instruments  scientifiques que les 4 joueurs doivent  <span className={styles.experienceSection__text___big}>faire fonctionner virtuellement</span>.  
                        </p>
                    </div>
                    <img className={styles.experienceSection__img___lunette} src={require('./imgs/photo-lunette.png')} />
                </div>
                <img className={styles.experienceSection__img} src={require('./imgs/mockup.png')} />
                <div className={styles.experienceSection__container}>
                    <div></div>
                    <p className={styles.experienceSection__text}>
                        Pour cela, ils devront utiliser une série d’outils virtuels. 
                        Grâce à eux, les objets, figés derrière les vitrines, retrouvent 
                        leur fonction d’usage.
                    </p>
                </div>
                <div className={styles.experienceSection__container}>
                    <div className={styles.experienceSection__paragraphe}>
                        <h4 className={styles.experienceSection___title}>Un scénario</h4>
                        <p className={styles.experienceSection__text}>
                            La progression dans le jeu n’est possible que par <span className={styles.experienceSection__text___big}>la coordination</span> des 4 membres du groupe.
                            <span className={styles.experienceSection__text___big}> Chaque joueur est indispensable</span> à la résolution des missions.
                        </p>
                        <article className={styles.experienceSection__relatedContent}>
                            <p className={styles.experienceSection__relatedContent___text}>
                                Une communication verbale et un œil affuté seront de mise. 
                            </p>
                        </article>
                    </div>
                    <img className={styles.experienceSection__img___hands} src={require('./imgs/hands.png')} />
                </div>
                <div className={styles.experienceSection__container}>
                    <img className={styles.experienceSection__img___hands} src={require('./imgs/notice.png')} />
                    <div className={styles.experienceSection__paragraphe}>
                        <h1 className={styles.experienceSection__title}>Un jeu au service d'un savoir</h1>
                        <h4 className={`${styles.experienceSection___title} ${styles.experienceSection__titlemt}`}>Vulgariser sans ennuyer</h4>
                        <p className={styles.experienceSection__text}>
                        Afin de réussir les missions, la compréhension des objets est essentielle. Pour cela, 
                        <span className={styles.experienceSection__text___big}> un contenu didactique</span> est accessible sur les smartphones des joueurs. 
                        </p>
                    </div>
                </div>
            </section>

            <section className={styles.objectiveSection}>
                <h1 className={styles.objectiveSection__title}>Les objectifs</h1>
                <div className={styles.objectiveSection__container}>
                    <div className={styles.objectiveSection__item}>
                        <h4 className={styles.objectiveSection__item_title}>Transmettre</h4>
                        <img className={styles.objectiveSection__item_img} src={require('./imgs/objectives/transmettre.png')} />
                    </div>
                    <div className={styles.objectiveSection__item}>
                        <h4 className={styles.objectiveSection__item_title}>Rompre la linéarité</h4>
                        <img className={styles.objectiveSection__item_img} src={require('./imgs/objectives/linearite.png')} />
                    </div>
                    <div className={styles.objectiveSection__item}>
                        <h4 className={styles.objectiveSection__item_title}>Faire de nouvelles <br></br> rencontres</h4>
                        <img className={styles.objectiveSection__item_img} src={require('./imgs/objectives/rencontres.png')} />
                    </div>
                </div>
            </section>

            <section className={styles.creditSection}>
                <h1 className={styles.creditSection__title}>Réalisé par</h1>
                <div className={styles.creditSection__container}>
                    <div className={styles.creditSection__item}>
                        <h4 className={styles.creditSection__item_name}>Aurélien <span className={styles.creditSection__item_name__thin}>Hémidy</span></h4>
                        <img className={styles.creditSection__item_img} src={require('./imgs/credits/aurelien.png')} />
                        <h5 className={styles.creditSection__item_job}>Développeur Front End</h5>
                        <p className={styles.creditSection__item_description}>
                            Expert dans l’art du CSS. Sans lui la Web app ne ressemblerait pas à ce qu’elle est aujourd’hui
                            avec une attention toute particulière au calage typographique.
                        </p>
                        <a href="#" className={styles.creditSection__item_website}><p>-</p></a>
                        <article>
                            <a href="#"><img className={styles.creditSection__item_imgSocial} src={require('./imgs/credits/socialmedia/insta.png')}/></a>
                            <a href="#"><img className={styles.creditSection__item_imgSocial} src={require('./imgs/credits/socialmedia/linkedin.png')}/></a>
                        </article>
                    </div>
                    <div className={styles.creditSection__item}>
                        <h4 className={styles.creditSection__item_name}>Arno <span className={styles.creditSection__item_name__thin}>Dubois</span></h4>
                        <img className={styles.creditSection__item_img} src={require('./imgs/credits/arno.png')} />
                        <h5 className={styles.creditSection__item_job}>Développeur Back End</h5>
                        <p className={styles.creditSection__item_description}>
                            Toujours au fait des dernières mises à jours et évolutions technologiques,
                            il a su lire entre les lignes de code pour déceler les moindres erreurs.
                        </p>
                        <a href="#" className={styles.creditSection__item_website}><p>arnodubo.is</p></a>
                        <article>
                            <a href="#"><img className={styles.creditSection__item_imgSocial} src={require('./imgs/credits/socialmedia/insta.png')}/></a>
                            <a href="#"><img className={styles.creditSection__item_imgSocial} src={require('./imgs/credits/socialmedia/linkedin.png')}/></a>
                        </article>
                    </div>
                    <div className={styles.creditSection__item}>
                        <h4 className={styles.creditSection__item_name}>Chloélia <span className={styles.creditSection__item_name__thin}>Breton</span></h4>
                        <img className={styles.creditSection__item_img} src={require('./imgs/credits/chloelia.png')} />
                        <h5 className={styles.creditSection__item_job}>UI designer</h5>
                        <p className={styles.creditSection__item_description}>
                            La médiation culturelle : un sujet qui lui tient à coeur ! Elle a apporté une
                            attention toute particulière à la cohérence de l’expérience.
                        </p>
                        <a href="#" className={styles.creditSection__item_website}><p >-</p></a>
                        <article>
                            <a href="#"><img className={styles.creditSection__item_imgSocial} src={require('./imgs/credits/socialmedia/insta.png')}/></a>
                            <a href="#"><img className={styles.creditSection__item_imgSocial} src={require('./imgs/credits/socialmedia/behance.png')}/></a>
                            <a href="#"><img className={styles.creditSection__item_imgSocial} src={require('./imgs/credits/socialmedia/linkedin.png')}/></a>
                        </article>
                    </div>
                    <div className={styles.creditSection__item}>
                        <h4 className={styles.creditSection__item_name}>Vincent <span className={styles.creditSection__item_name__thin}>Calas</span></h4>
                        <img className={styles.creditSection__item_img} src={require('./imgs/credits/vincent.png')} />
                        <h5 className={styles.creditSection__item_job}>UI designer</h5>
                        <p className={styles.creditSection__item_description}>
                            Attaché au dessin, il a mis toute son 
                            énergie pour un rendu fin et immersif. 
                            After Effects a été son meilleur allié durant le projet. 
                        </p>
                        <a href="#" className={styles.creditSection__item_website}><p >vincentcalas.com</p></a>
                        <article>
                            <a href="#"><img className={styles.creditSection__item_imgSocial} src={require('./imgs/credits/socialmedia/insta.png')}/></a>
                            <a href="#"><img className={styles.creditSection__item_imgSocial} src={require('./imgs/credits/socialmedia/behance.png')}/></a>
                            <a href="#"><img className={styles.creditSection__item_imgSocial} src={require('./imgs/credits/socialmedia/linkedin.png')}/></a>
                        </article>
                    </div>
                </div>
                <img className={styles.creditSection__item_imgLogo} src={require('./imgs/credits/logo-gobelins.png')}/>
                <p className={styles.creditSection__creditImg}>sources photographiques : artsandculture.google.com</p>
            </section>
        </>
    );
}
