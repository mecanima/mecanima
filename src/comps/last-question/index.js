import React from "react";
import { useObserver } from "mobx-react-lite";
import { useRouter } from "next/router";

import styles from "./last-question.module.scss";

import RoomStore from "../../store/room";

const LastQuestion = (props) => {
    const router = useRouter();
    let isCompleted = false;
    const sendText = (evt) => {
        RoomStore.setFinalChallenge(0, evt.target.value);
    };
    useObserver(() => {
        if (RoomStore.finalChallenge[0].found === true) {
            isCompleted = true;
            setTimeout(() => {
                RoomStore.setGameState(props.destination);
                router.push(`/${RoomStore.gameName}-${props.destination}`);
            }, 3500);
        }
    });
    return useObserver(() => (
        <div className={styles.FirstQuestionContainer}>
            <h4>{props.title}</h4>
            <p>{props.text}</p>
            <p className={styles.questionMark}>{props.question}</p>
            <div className={styles.inputContainer}>
                <img
                    src={require("./imgs/curtain.png")}
                    className={styles.curtain}
                />
                <img
                    src={require("./imgs/valide.png")}
                    className={`${styles.check} ${
                        isCompleted ? styles.checked : ""
                    }`}
                />
                <input
                    className={`${styles.input} ${
                        isCompleted ? styles.active : ""
                    }`}
                    type={props.type ? props.type : "text"}
                    onChange={sendText}
                    value={RoomStore.finalChallenge[0].value}
                ></input>
                <picture>
                    <source
                        srcSet={require("./imgs/input.png?webp")}
                        type="image/webp"
                    />
                    <source
                        srcSet={require("./imgs/input.png")}
                        type="image/png"
                    />
                    <img
                        src={require("./imgs/input.png")}
                        className={`${styles.img} ${
                            isCompleted ? styles.height : ""
                        }`}
                    />
                </picture>
            </div>
        </div>
    ));
};

export default LastQuestion;
