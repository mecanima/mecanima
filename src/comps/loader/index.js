import React from "react";
import styles from "./loader.module.scss";

export default function Loader() {
    return (
        <section className={styles.loader}>
            <img src={require("./loading.png?resize&size=150")} />
            <h1>chargement...</h1>
        </section>
    );
}
