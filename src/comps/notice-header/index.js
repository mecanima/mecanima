import React from "react";
import styles from "./notice-header.module.scss";

const Noticeheader = props => {
    if(props.main) {
        return (
            <div className={styles.headerContainerMain}>
                <h1>Notice</h1>
                <p>{props.tool}</p>
            </div>
        )
    } 
    else {
        return (
            <div className={styles.headerContainerFixed}>
                <h1>{props.tool}</h1>
            </div>
        )
    }
};

export default Noticeheader;
