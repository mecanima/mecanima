import React from "react";
import styles from "./moons.module.scss";

import { moons } from "../../utils/moons";

const Moons = (props) => {
    return (
        <div className={styles.moons}>
            {moons.map((elm) => (
                <img
                    key={elm.id}
                    className={
                        props.moonSelected === elm.id ? "" : styles.white
                    }
                    src={elm.image.thumb}
                />
            ))}
        </div>
    );
};

export default Moons;
