import React from "react";
import styles from './missionDescription.module.scss';

const MissionDescription = props => {
    return (
       <section className={styles.missionContainer}>
           <picture>
                <source srcSet={require("./background.png?webp")} type="image/webp" />
                <source srcSet={require("./background.png")} type="image/png" />
                <img src={require("./background.png")} />
            </picture>
            <p className={styles.title}><span>{`${props.noMission ? "" : 'Mission'}`} {props.missionNum}</span> {props.title}</p>
            <p className={styles.description}>{props.desc}</p>
       </section>
    );
};

export default MissionDescription;
