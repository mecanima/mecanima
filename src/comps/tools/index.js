import React, { useRef, useState, useEffect } from "react";
import dynamic from "next/dynamic";
import Flickity from "react-flickity-component";

import Tool from "comps/tool";

import RoomStore from "store/room";

import styles from "./tools.module.scss";
import { useRouter } from "next/router";

// import { grantPermission } from "utils/compass";
import usePrevious from "utils/usePrevious";

const ReactTextTransition = dynamic(() => import("react-text-transition"), {
    ssr: false,
});

const flickityOptions = {
    wrapAround: true,
    prevNextButtons: false,
    pageDots: false,
    initialIndex: 1,
    selectedAttraction: 0.2,
    friction: 0.8,
};

export default function Tools() {
    const router = useRouter();
    const flickity = useRef(null);
    const [currentSlide, setCurrentSlide] = useState(
        flickityOptions.initialIndex
    );
    const oldIndex = usePrevious(currentSlide);
    const [transitionSide, setTransitionSide] = useState("down");
    useEffect(() => {
        if (currentSlide === RoomStore.tools.length - 1 && oldIndex === 0) {
            setTransitionSide("down");
        } else if (
            currentSlide === 0 &&
            oldIndex === RoomStore.tools.length - 1
        ) {
            setTransitionSide("up");
        } else if (currentSlide > oldIndex) {
            setTransitionSide("up");
        } else if (currentSlide < oldIndex) {
            setTransitionSide("down");
        }
    }, [currentSlide]);
    const setupFlickity = (ref) => {
        flickity.current = ref;
        flickity.current.on("change", (index) => {
            setCurrentSlide(index);
        });
        flickity.current.on("staticClick", (evt, index, cellElm, cellIndex) => {
            pickupTool(RoomStore.tools[cellIndex].name);
        });
    };

    const pickupTool = (tool) => {
        // const continueToTool = () => {
        RoomStore.pickTool(tool, true).then(() => router.push(`/is-${tool}`));
        // };
        // if (tool === "compass" || tool === "telescope") {
        //     if (
        //         window.DeviceOrientationEvent &&
        //         typeof window.DeviceOrientationEvent.requestPermission ===
        //             "function"
        //     ) {
        //         grantPermission().then(continueToTool);
        //     } else {
        //         continueToTool();
        //     }
        // } else {
        // continueToTool();
        // }
    };
    return (
        <>
            <span className={styles.counter}>
                <ReactTextTransition
                    className={styles.textTransition}
                    text={currentSlide + 1}
                    direction={transitionSide}
                ></ReactTextTransition>
                <span className={styles.line}></span>
                <span className={styles.totalCounter}>
                    {RoomStore.tools.length}
                </span>
            </span>
            <style global jsx>{`
                .is-selected {
                    transform: scale(1.4);
                }
            `}</style>
            <div className={styles.carouselWrapper}>
                <Flickity
                    className={styles.carousel}
                    flickityRef={setupFlickity}
                    options={flickityOptions}
                    static
                    disableImagesLoaded
                >
                    {RoomStore.tools.map((elm) => (
                        <div key={elm.name} className={styles.slide}>
                            <Tool
                                tool={elm.name}
                                animationDelay={Math.random() * 2 + "s"}
                                selected={currentSlide + 1}
                            ></Tool>
                        </div>
                    ))}
                </Flickity>
            </div>
        </>
    );
}
