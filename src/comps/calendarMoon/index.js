import React from "react";
import styles from "./calendarMoon.module.scss";

import { moons } from "../../utils/moons"

const CalendarMoon = props => {
    const selectedMoon = moons[props.selected];
    return (
        <>
            <img src={props.imgSrc} />
            <h4 className={`${styles.title} ${props.selected === props.moon ? "" : styles.noText}`}>{selectedMoon.step}</h4>
            <h6 className={`${styles.date} ${props.selected === props.moon ? "" : styles.noText}`}>{selectedMoon.date}</h6>
        </>
    );
};

export default CalendarMoon;
