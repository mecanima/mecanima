import React, { useState, useEffect } from "react";
import styles from "./mission-btn.module.scss";

import RoomStore from "store/room";

import { observer } from "mobx-react-lite";

const MissionBtn = (props) => {
    const [isCompleted, setIsCompleted] = useState(false);
    const [runAnimation, setRunAnimation] = useState(false);
    const [firstRender, setFirstRender] = useState(true);
    let existanceChecks =
        props.id < RoomStore.finalAnswers.length &&
        typeof RoomStore.finalAnswers[props.id] !== "undefined";
    const setCompletedState = () => {
        setIsCompleted(
            existanceChecks &&
                typeof RoomStore.finalAnswers[props.id].found !== "undefined"
                ? RoomStore.finalAnswers[props.id].found
                : false
        );
    };
    useEffect(() => {
        if (firstRender === true) {
            setCompletedState();
        }
    }, []);
    const sendText = (evt) => {
        const missionAnswer = evt.target.value;
        RoomStore.setFinalAnswer(props.id, missionAnswer);
    };

    useEffect(() => {
        setFirstRender(false);
    });
    useEffect(() => {
        if (
            existanceChecks &&
            RoomStore.finalAnswers[props.id].found === true &&
            firstRender === false
        ) {
            setRunAnimation(true);
            setTimeout(() => setIsCompleted(true), 3500);
        }
    }, [existanceChecks && RoomStore.finalAnswers[props.id].found]);

    // if (props.mode === "unlocked") {
    //     return (
    //         <button
    //             className={styles.missionScenarioMenu}
    //             onClick={props.onClick}
    //         >
    //             <p className={styles.text}>
    //                 Rendez-vous à la mission {props.id + 1}
    //             </p>
    //             <img

    //                 src={
    //                     // props.active
    //                         // ? require("./imgs/fond-2.png")
    //                         require("./imgs/fond-1.png")
    //                 }
    //             />
    //         </button>
    //     );
    // } else
    if (props.mode === "locked" || props.mode === "unlocked") {
        return (
            <button
                className={styles.missionScenarioMenu}
                disabled={props.mode === "locked"}
                onClick={props.onClick}
            >
                <p className={styles.text}>
                    Rendez-vous à la mission {props.id + 1}
                </p>
                <img
                    className={`${
                        props.mode === "locked" ? styles.locked : ""
                    }`}
                    src={require("./imgs/fond-2.png")}
                />
            </button>
        );
    } else if (props.mode === "current") {
        return (
            <>
                {isCompleted ? (
                    <button className={styles.notAnsweredContainer} disabled>
                        <p>Vous avez réussi la mission {props.id + 1}</p>
                        <img
                            className={styles.locked}
                            src={require("./imgs/fond-2.png")}
                        />
                    </button>
                ) : (
                    <button
                        className={styles.missionBtn}
                        onClick={props.onClick}
                    >
                        <p className={styles.title}>
                            <span>Mission {props.id + 1}</span>
                            {props.title}
                        </p>
                        <p className={styles.description}>{props.hint}</p>
                        <img src={require("./imgs/fond-irregulier.png")} />
                        <div className={styles.inputContainer}>
                            <img
                                src={require("./imgs/curtain.png")}
                                className={styles.curtain}
                            />
                            <img
                                src={require("./imgs/champ_bas.png")}
                                className={`${styles.curtain} ${styles.champ_bas}`}
                            />
                            <img
                                src={require("./imgs/valide.png")}
                                className={`${styles.check} ${
                                    runAnimation ? styles.checked : ""
                                }`}
                            />
                            <input
                                className={`${styles.input} ${
                                    runAnimation ? styles.active : ""
                                }`}
                                type="number"
                                placeholder="_ _"
                                onChange={sendText}
                                value={RoomStore.finalAnswers[props.id].value}
                                // maxLength="2"
                            ></input>
                            <picture>
                                <source
                                    srcSet={require("./imgs/champ.png?webp")}
                                    type="image/webp"
                                />
                                <source
                                    srcSet={require("./imgs/champ.png")}
                                    type="image/png"
                                />
                                <img
                                    src={require("./imgs/champ.png")}
                                    className={`${styles.img} ${
                                        runAnimation ? styles.height : ""
                                    }`}
                                />
                            </picture>
                        </div>
                    </button>
                )}
            </>
        );
    }
};

export default observer(MissionBtn);
