import React, { useState, useEffect, useRef } from "react";
import { useFrame, useThree } from "react-three-fiber";
import Head from "next/head";

import {
    CubeTextureLoader,
    LinearMipMapLinearFilter,
    TextureLoader,
    Vector3,
} from "three";

import { updateCamera } from "utils/compass";
import { moonLocations } from "utils/moons";

// const panoImage = require("./panorama.png");
const cubemap = [
    require("./panorama/bande2.png"),
    require("./panorama/bande4.png"),
    require("./panorama/bande5.png"),
    require("./panorama/bande6.png"),
    require("./panorama/bande3.png"),
    require("./panorama/bande1.png"),
];

const moons = [
    require("utils/moons/imgs/00_nouvelle_lune.png?size=256"),
    require("utils/moons/imgs/01_premier_croissant.png?size=256"),
    require("utils/moons/imgs/02_premier_quartier.png?size=256"),
    require("utils/moons/imgs/03_lune_gibbeuse.png?size=256"),
    require("utils/moons/imgs/04_pleine_lune.png?size=256"),
    require("utils/moons/imgs/05_lune_gibbeuse.png?size=256"),
    require("utils/moons/imgs/06_dernier_quartier.png?size=256"),
    require("utils/moons/imgs/07_dernier_croissant.png?size=256"),
];

const goodMoon = require("utils/moons/imgs/02_premier_quartier_chiffre.png?size=256");

const moonHalo = require("./halo.png");

// const imageLoadToPromise = (image) =>
//     new Promise((resolve) => (image.onload = resolve));

export default function TelescopeCanvas(props) {
    const [initialized, setInitialized] = useState(false);
    const [moonPosition, setMoonPosition] = useState(0);
    const skyTexture = useRef(null);
    const moonGroup = useRef(null);
    const moonMap = useRef(null);
    const moonHaloMap = useRef(null);

    const threeData = useThree();
    window.scene = threeData.scene;
    const threeControl = useRef(null);

    //     const glowMaterial = {
    //         uniforms: {
    //             c: { type: "f", value: 0.6 },
    //             p: { type: "f", value: 6 },
    //             glowColor: { type: "c", value: new Color(0xffffff) },
    //             viewVector: { type: "v3", value: threeData.camera.position },
    //         },
    //         vertexShader: `
    //     uniform vec3 viewVector;
    // uniform float c;
    // uniform float p;
    // varying float intensity;
    // void main()
    // {
    //     vec3 vNormal = normalize( normalMatrix * normal );
    // 	vec3 vNormel = normalize( normalMatrix * viewVector );
    // 	intensity = pow( c - dot(vNormal, vNormel), p );

    //     gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    // }
    //     `,
    //         fragmentShader: `
    //     uniform vec3 glowColor;
    // varying float intensity;
    // void main()
    // {
    // 	vec3 glow = glowColor * intensity;
    //     gl_FragColor = vec4( glow, 1.0 );
    // }
    //     `,
    //         side: FrontSide,
    //         blending: AdditiveBlending,
    //         transparent: true,
    //     };

    useFrame(() => {
        // console.log(props.orientation);
        updateCamera(threeData.camera, props.orientation);
    });

    const refreshMoon = async () => {
        let moonToLoad = null;
        if (
            props.telescopeState.lenses === true &&
            props.telescopeState.calendar === 2
        ) {
            moonToLoad = goodMoon.src;
        } else {
            moonToLoad = moons[props.telescopeState.calendar].src;
        }
        const moonTexture = await new TextureLoader().loadAsync(moonToLoad);
        moonMap.current = moonTexture;
        const yAxis = new Vector3(0, 1, 0);
        if (moonGroup.current) {
            moonGroup.current.rotateOnWorldAxis(
                yAxis,
                moonPosition * (Math.PI / 180)
            );
            const newPosition = moonLocations[props.telescopeState.calendar];
            setMoonPosition(newPosition);
            moonGroup.current.rotateOnWorldAxis(
                yAxis,
                -newPosition * (Math.PI / 180)
            );
            setMoonPosition(newPosition);
        }
    };
    useEffect(() => {
        return () => {
            if (threeControl.current !== null) threeControl.current.dispose();
        };
    }, []);
    const loadSkybox = async () => {
        const texture = await new CubeTextureLoader().loadAsync(cubemap);
        texture.anisotropy = threeData.gl.capabilities.getMaxAnisotropy();
        texture.minFilter = LinearMipMapLinearFilter;
        skyTexture.current = texture;
        threeData.scene.background = skyTexture.current;
        moonHaloMap.current = await new TextureLoader().loadAsync(moonHalo);
    };
    useEffect(() => {
        refreshMoon();
    }, [props.telescopeState.calendar, props.telescopeState.lenses]);
    const initializeViewer = async () => {
        console.log("initializing");
        if (typeof window === "undefined") return;
        await loadSkybox();
        setInitialized(true);
    };
    useEffect(() => {
        const startViewer = async () => {
            if (props.startViewer === true) {
                await initializeViewer();
                // moonGroup.current.lookAt(Vector3(0, 0, 0));
                await refreshMoon();
            }
        };
        startViewer();
    }, [props.startViewer]);

    return (
        (initialized && (
            <>
                <Head>
                    {cubemap.map((image) => (
                        <link
                            rel="preload"
                            key={image}
                            href={image}
                            as="image"
                        />
                    ))}
                    {moons.map((image) => (
                        <link
                            rel="preload"
                            key={image}
                            href={image}
                            as="image"
                        />
                    ))}
                </Head>
                <group ref={moonGroup}>
                    <mesh
                        position={[0, 80, -150]}
                        scale={[1, 1, 1]}
                        rotation={[(Math.PI / 180) * 30, 0, 0]}
                    >
                        <planeBufferGeometry
                            attach="geometry"
                            args={[20, 20]}
                        />
                        <meshBasicMaterial
                            attach="material"
                            map={moonMap.current}
                            precision="highp"
                            transparent={true}
                        />
                    </mesh>
                    <mesh
                        position={[0, 80.6, -151]}
                        scale={[1.25, 1.25, 1.25]}
                        rotation={[(Math.PI / 180) * 30, 0, 0]}
                    >
                        <planeBufferGeometry
                            attach="geometry"
                            args={[20, 20]}
                        />
                        <meshBasicMaterial
                            attach="material"
                            map={moonHaloMap.current}
                            precision="highp"
                            transparent={true}
                        />
                    </mesh>
                    {/* <mesh
                        position={[0, 81, -151]}
                        scale={[0.5, 0.5, 0.5]}
                        // rotation={[0, Math.PI * 0.8, 0]}
                    >
                        <circleBufferGeometry
                            attach="geometry"
                            args={[20, 50]}
                        />
                        <shaderMaterial {...glowMaterial} />
                    </mesh> */}
                </group>
                {process.env.ENV !== "production" && (
                    <mesh position={[0, 0, 0]}>
                        <boxBufferGeometry
                            attach="geometry"
                            args={[100, 100, 100, 4, 4, 4]}
                        />
                        <meshBasicMaterial
                            attach="material"
                            color={0xff00ff}
                            wireframe
                        />
                    </mesh>
                )}
            </>
        )) ||
        null
    );
}
