import React from "react";
// import Link from "next/link";
import styles from "./container.module.scss";

const Container = (props) => (
    <div
        className={`${styles.container} ${
            props.fullHeight ? styles.fullHeight : ""
        } ${props.blue ? styles.backgroundBlue : ""} ${
            props.className ? props.className : ""
        }
        ${props.fullVH ? styles.fullVH : ""}
        `}
    >
        {props.children}
    </div>
);

export default Container;
