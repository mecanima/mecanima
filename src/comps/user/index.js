import React from "react";
import styles from "./user.module.scss";

const CharactersImages = {
    galilee: require("./images/galilee.png?resize&size=300").src,
    ferdinand: require("./images/ferdinand.png?resize&size=300").src,
    eugene: require("./images/pascal.png?resize&size=300").src,
    wilhelm: require("./images/whilelm.png?resize&size=300").src,
};

const User = (props) => {
    // console.log(CharactersImages[props.image])
    if (props.state === "currentUser") {
        return (
            <div className={styles.containerUserImage}>
                <div className={styles.userImage}>
                    <img src={CharactersImages[props.image]} />
                    {/* <picture>
                        <source
                            srcSet={require("./background.png?webp")}
                            type="image/webp"
                        />
                        <source
                            srcSet={require("./background.png")}
                            type="image/png"
                        />
                        <img
                            className={styles.imgBackground}
                            src={require("./background.png")}
                        />
                    </picture> */}
                </div>
                <p>
                    Vous êtes <br></br><span>{props.name}</span> <br></br> Vous avez rejoint la
                    partie
                </p>
            </div>
        );
    } else if (props.state === "otherUsers") {
        return (
            <div className={styles.otherPlayerContainer}>
                <div className={styles.otherPlayer}>
                    <img src={CharactersImages[props.image]} />
                    {/* <img
                        className={styles.imgBackground}
                        src={require("./background.png")}
                    /> */}
                </div>
                <p>
                    <span>{props.name}</span> <br></br> a rejoint la partie
                </p>
            </div>
        );
    } else if (props.state === "noUser") {
        return (
            <div
                key={props.index}
                className={styles.otherPlayerContainerWaiting}
            >
                <div className={styles.otherPlayer}>
                    {/* <img src={require('./galil-e.png')} /> */}
                    <img
                        className={styles.imgBackground}
                        src={require("./background.png")}
                    />
                </div>
                <p>
                    <span>{props.name}</span>
                    <br></br> en attente
                </p>
            </div>
        );
    }
};

export default User;
