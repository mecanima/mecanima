import React from "react";
import styles from "./notice-btn.module.scss";
import { useRouter } from "next/router";

const NoticeBtn = props => {
    const router = useRouter();
    return (
        <button
            className={styles.noticeBtn}
            onClick={props.onClick}
        >
            <img src={require("./notice.png")} />
            Notice
        </button>
    );
};

export default NoticeBtn;
