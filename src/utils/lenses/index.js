export const lensesTab = [ 
    {
        step: "convergente_plat",
        img: {
            src: require("./imgs/convergente-plat.png"),
            webp: require("./imgs/convergente-plat.png?webp"),
        },
    },
    {
        step: "convergent",
        img: {
            src: require("./imgs/convergent.png"),
            webp: require("./imgs/convergent.png?webp"),
        },
    },
    {
        step: "divergent",
        img: {
            src: require("./imgs/divergent.png"),
            webp: require("./imgs/divergent.png?webp"),
        },
    },
    {
        step: "convergente_plat",
        img: {
            src: require("./imgs/convergente-plat.png"),
            webp: require("./imgs/convergente-plat.png?webp"),
        },
    },
    {
        step: "convergent",
        img: {
            src: require("./imgs/convergent.png"),
            webp: require("./imgs/convergent.png?webp"),
        },
    },
    {
        step: "divergent",
        img: {
            src: require("./imgs/divergent.png"),
            webp: require("./imgs/divergent.png?webp"),
        },
    },
    {
        step: "convergente_plat",
        img: {
            src: require("./imgs/convergente-plat.png"),
            webp: require("./imgs/convergente-plat.png?webp"),
        },
    },
    {
        step: "convergent",
        img: {
            src: require("./imgs/convergent.png"),
            webp: require("./imgs/convergent.png?webp"),
        },
    },
    {
        step: "divergent",
        img: {
            src: require("./imgs/divergent.png"),
            webp: require("./imgs/divergent.png?webp"),
        },
    },
];