export const otherChapters = {
    oculaire: {
        title: "L'oculaire",
        img: require("./imgs/oculaire.png"),
        route: "mission1-oculaire"
    },
    objectif: {
        title: "L'objectif",
        img: require("./imgs/objectif.png"),
        route: "mission1-objectif"
    },
    astre: {
        title: "Les astres",
        img: require("./imgs/lune.png"),
        route: "mission1-astre"
    },
    fonction: {
        title: "Le fonctionnement",
        img: require("./imgs/fonction.png"),
        route: "end-fonction"
    },
    machine: {
        title: "Une machine à calculer",
        img: require("./imgs/machine.png"),
        route: "end-machine"
    },
    money: {
        title: "Le système monétaire",
        img: require("./imgs/money.png"),
        route: "end-money"
    },
}