import RoomStore from "../store/room";
export function getRoomRoute(type) {
    if (RoomStore.gameName && RoomStore.gameState) {
        return `/${RoomStore.gameName}-${RoomStore.gameState}`;
    } else if (type === "connect") {
        return "/room";
    } else if (type === "room") {
        if (RoomStore.roomState === "started") {
            if (RoomStore.gameName && RoomStore.roomState === "started") {
                return `/${RoomStore.gameName}-starting-screen`;
            } else {
                return "";
            }
        }
    } else {
        return false;
    }
}
