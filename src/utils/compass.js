import { Euler, MathUtils, Quaternion, Vector3 } from "three";
import KalmanFilter from "kalmanjs";

let storedHandler = null;
let absoluteOrientation = {
    alpha: 0,
    beta: 0,
    gamma: 0,
};
let oldData = {
    alpha: 0,
    beta: 0,
    gamma: 0,
};
const kalmans = {
    alpha: new KalmanFilter(),
    beta: new KalmanFilter(),
    gamma: new KalmanFilter(),
};
const rotationHandling = (evt) => {
    absoluteOrientation.beta -= getOrientDelta(
        oldData.beta,
        evt.beta,
        -180,
        180
    );
    absoluteOrientation.gamma -= oldData.gamma - evt.gamma;
    // absoluteOrientation.gamma -= getOrientDelta(
    //     oldData.gamma,
    //     evt.gamma,
    //     -90,
    //     90
    // );
    oldData.beta = evt.beta;
    oldData.gamma = evt.gamma;
    if (evt.absolute) {
        absoluteOrientation.alpha -= getOrientDelta(
            oldData.alpha,
            evt.alpha,
            0,
            360
        );
        oldData.alpha = evt.alpha;
    } else if (evt.webkitCompassHeading) {
        absoluteOrientation.alpha += getOrientDelta(
            oldData.alpha,
            evt.webkitCompassHeading,
            0,
            360
        );
        oldData.alpha = evt.webkitCompassHeading;
    } else {
        // alert(
        //     "Votre téléphone ne peut pas récupérer les données de la boussole. Merci de réessayer avec Google Chrome ou Safari sur iOS."
        // );
        console.error(evt);
        return storedHandler(false);
    }
    return storedHandler({
        alpha: (kalmans.alpha.filter(absoluteOrientation.alpha) + 360) % 360,
        beta:
            // ((kalmans.beta.filter(absoluteOrientation.beta) + 180) % 360) - 180,
            kalmans.beta.filter(absoluteOrientation.beta),
        gamma:
            // ((kalmans.gamma.filter(absoluteOrientation.gamma) + 90) % 180) - 90,
            kalmans.gamma.filter(absoluteOrientation.gamma),
    });
};
const setObjectQuaternion = (function () {
    const zee = new Vector3(0, 0, 1);
    const euler = new Euler();
    const q0 = new Quaternion();
    const q1 = new Quaternion(-Math.sqrt(0.5), 0, 0, Math.sqrt(0.5)); // - PI/2 around the x-axis

    return function (quaternion, alpha, beta, gamma, orient) {
        euler.set(beta, alpha, -gamma, "YXZ"); // 'ZXY' for the device, but 'YXZ' for us
        quaternion.setFromEuler(euler); // orient the device
        quaternion.multiply(q1); // camera looks out the back of the device, not the top
        quaternion.multiply(q0.setFromAxisAngle(zee, -orient)); // adjust for screen orientation
    };
})();
export const updateCamera = (camera, orientation) => {
    const alpha = orientation.alpha ? MathUtils.degToRad(orientation.alpha) : 0; // Z
    const beta = orientation.beta ? MathUtils.degToRad(orientation.beta) : 0; // X'
    const gamma = orientation.gamma ? MathUtils.degToRad(orientation.gamma) : 0; // Y''

    setObjectQuaternion(camera.quaternion, alpha, beta, gamma, 0);
};
export const grantPermission = () => {
    return new Promise((resolve, reject) => {
        window.DeviceOrientationEvent.requestPermission()
            .then((status) => {
                if (status === "granted") {
                    console.log("Sensors permission granted");
                    resolve();
                } else {
                    console.error("Permissions status: " + status);
                    alert(
                        "Si tu vois ça, rappelle moi de l'enlever. C'est que Safari a pas détecté que le clic était à l'origine de l'action (ce qui est logique en fait, mais chiant)"
                    );
                    reject();
                }
                window.removeEventListener("click", grantPermission);
            })
            .catch((err) =>
                console.error("An error occured when asking permission: ", err)
            );
    });
};
export const startCompass = (handler) => {
    storedHandler = handler;
    if ("ondeviceorientationabsolute" in window) {
        window.addEventListener(
            "deviceorientationabsolute",
            rotationHandling,
            true
        );
    } else {
        if (
            window.DeviceOrientationEvent &&
            typeof window.DeviceOrientationEvent.requestPermission ===
                "function"
        ) {
            window.addEventListener("click", grantPermission);
            // const askForPermission = () => {
            //     document.removeEventListener("click", askForPermission);
            // };
            // console.log(
            //     "Enabling Safari mode. Asking permission to use sensors"
            // );
            // document.addEventListener("click", () => askForPermission);
        }
        window.addEventListener("deviceorientation", rotationHandling, true);
        oldData = { alpha: 0, beta: 0, gamma: 0 };
        absoluteOrientation = {
            alpha: 0,
            beta: 0,
            gamma: 0,
        };
    }
};

export const stopCompass = () => {
    if ("ondeviceorientationabsolute" in window) {
        window.removeEventListener(
            "deviceorientationabsolute",
            rotationHandling,
            true
        );
    } else {
        window.removeEventListener("deviceorientation", rotationHandling, true);
    }
};

// export const normalizeYaw = (yaw) => {
//     if (yaw >= 0 && yaw < 180) {
//         return -yaw;
//     } else {
//         return -(yaw - 360);
//     }
// };
export const getOrientDelta = (oldOrient, newOrient, min, max) => {
    let midVal = (min + max) / 2;
    let deltaQuarter = Math.abs(midVal - min) / 2;
    let midValBot = midVal - deltaQuarter;
    let midValTop = midVal + deltaQuarter;
    if (min === -90) console.log(newOrient);
    // ie. 355° --> 4°
    if (oldOrient >= midValTop && newOrient < midValBot) {
        return oldOrient - max + (min - newOrient);
    }
    // ie. 6° --> 346°
    else if (oldOrient <= midValBot && newOrient > midValTop) {
        return oldOrient - min + (max - newOrient);
    } else {
        return oldOrient - newOrient;
    }
};
