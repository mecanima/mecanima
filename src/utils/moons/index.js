export const moonLocations = [0, 45, 90, 135, 180, 225, 270, 315];

export const moons = [
    {
        step: "Nouvelle lune",
        date: "21 avril",
        id: 0,
        image: {
            big: require("./imgs/00_nouvelle_lune.png?resize&size=700"),
            thumb: require("./imgs/00_nouvelle_lune.png?resize&size=100"),
        },
    },
    {
        step: "Premier croissant",
        date: "22 avril",
        id: 1,
        image: {
            big: require("./imgs/01_premier_croissant.png?resize&size=700"),
            thumb: require("./imgs/01_premier_croissant.png?resize&size=100"),
        },
    },
    {
        step: "Premier Quartier",
        date: "23 avril",
        id: 2,
        image: {
            big: require("./imgs/02_premier_quartier.png?resize&size=700"),
            thumb: require("./imgs/02_premier_quartier.png?resize&size=100"),
        },
    },
    {
        step: "Lune gibbeuse",
        date: "24 avril",
        id: 3,
        image: {
            big: require("./imgs/03_lune_gibbeuse.png?resize&size=700"),
            thumb: require("./imgs/03_lune_gibbeuse.png?resize&size=100"),
        },
    },
    {
        step: "Pleine lune",
        date: "25 avril",
        id: 4,
        image: {
            big: require("./imgs/04_pleine_lune.png?resize&size=700"),
            thumb: require("./imgs/04_pleine_lune.png?resize&size=100"),
        },
    },
    {
        step: "Lune gibbeuse",
        date: "26 avril",
        id: 5,
        image: {
            big: require("./imgs/05_lune_gibbeuse.png?resize&size=700"),
            thumb: require("./imgs/05_lune_gibbeuse.png?resize&size=100"),
        },
    },
    {
        step: "Dernier quartier",
        date: "27 avril",
        id: 6,
        image: {
            big: require("./imgs/06_dernier_quartier.png?resize&size=700"),
            thumb: require("./imgs/06_dernier_quartier.png?resize&size=100"),
        },
    },
    {
        step: "Dernier croissant",
        date: "28 avril",
        id: 7,
        image: {
            big: require("./imgs/07_dernier_croissant.png?resize&size=700"),
            thumb: require("./imgs/07_dernier_croissant.png?resize&size=100"),
        },
    },
];
