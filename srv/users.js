class User {
    constructor(id) {
        this.id = id;
        this.character = null;
    }
    set(key, data) {
        this[key] = data;
    }
    setCharacter(character) {
        this.character = character;
    }
    setSocket(socket) {
        this.socket = socket;
    }
    setRoom(room) {
        this.room = room;
    }
    export() {
        const toExport = { ...this };
        delete toExport.socket;
        toExport.room = this.room ? this.room.id : undefined;
        return toExport;
    }
}

module.exports = User;
