const User = require("./users");
const games = require("./games");

class RoomManager {
    constructor() {
        this.rooms = [];
        this.disconnectedUsers = [];
    }
    join(roomId, user, socket) {
        let room;
        let gameName;
        switch (roomId.charAt(0)) {
            case "O":
                gameName = "instrumentsScientifiques";
                break;
            default:
                console.error(
                    user.id +
                        " a tenté d'ouvrir une salle avec un code invalide : " +
                        roomId
                );
                return false;
        }
        if (!this.exists(roomId)) {
            room = this.create(roomId);
            console.log(
                `La salle ${roomId} n'existait pas et a donc été crée avec le jeu ${gameName}`
            );
            room.setGame(gameName);
        } else {
            room = this.get(roomId);
        }
        room.addUser(user);
        user.setRoom(room);
        socket.join(room);
        console.log(user.id + " a rejoint la salle " + roomId);
        socket.to(room).broadcast.emit("room", {
            action: "join",
            payload: { user: user.export() },
        });
        return room.export();
    }
    quit(roomId, user, socket) {
        if (this.exists(roomId)) {
            const room = this.get(roomId);
            room.removeUser(user);
            user.setRoom(undefined);
            socket.leave(room);
            console.log(user.id + " a quitté la salle " + roomId);
            socket.to(roomId).broadcast.emit("room", {
                action: "quit",
                payload: { user: user.export() },
            });
        }
    }
    exists(roomId) {
        return this.rooms.some((elm) => elm.id === roomId);
    }
    create(id) {
        const room = new Room(id);
        this.rooms.push(room);
        return room;
    }
    get(id) {
        return this.rooms.find((elm) => elm.id === id);
    }
    start(room, socket) {
        room.state = "started";
        socket.to(room).broadcast.emit("room", {
            action: "start",
            payload: { room: room.export() },
        });
        return room.export();
    }
    roomAction(data, socket, returnFunction) {
        let returnData;
        if (typeof socket.user === "undefined") {
            console.error(
                "Des actions de salle ont été reçues avant le setup de l'utilisateur !"
            );
            if (typeof returnFunction === "function") returnFunction(false);
            return false;
        }
        switch (data.action) {
            case "join":
                returnData = this.join(
                    data.payload.roomId,
                    socket.user,
                    socket
                );
                break;
            case "quit":
                returnData = this.quit(
                    data.payload.roomId,
                    socket.user.id,
                    socket
                );
                break;
            case "start":
                returnData = this.start(socket.user.room, socket);
                break;
            case "delete":
                this.quit(data.payload.roomId, socket.user, socket);
                this.deleteRoom(data.payload.roomId);
                console.log(`La salle ${data.payload.roomId} a été supprimée`);
        }
        if (typeof returnFunction === "function") returnFunction(returnData);
    }
    gameAction(data, socket, returnFunction, io) {
        if (typeof socket.user === "undefined") {
            console.error(
                "Des actions de jeu ont été reçues avant le setup de l'utilisateur !"
            );
            if (typeof returnFunction === "function") returnFunction(false);
            return false;
        } else if (typeof socket.user.room === "undefined") {
            console.error(
                "Des actions de jeu ont été reçues, mais l'utilisateur n'a pas rejoint de salle !"
            );
            if (typeof returnFunction === "function") returnFunction(false);
            return false;
        } else {
            socket.user.room.gameAction(data, socket, returnFunction, io);
        }
    }
    setupUser(id, socket, returnFunction) {
        const oldUser = this.disconnectedUsers.find((user) => user.id === id);
        const oldUserIndex = this.disconnectedUsers.findIndex(
            (user) => user.id === id
        );
        if (oldUserIndex >= 0) {
            socket.user = oldUser;
            socket.user.setSocket(socket);
            this.disconnectedUsers.splice(oldUserIndex, 1);
        } else {
            socket.user = new User(id);
            socket.user.setSocket(socket);
        }
        if (typeof returnFunction === "function")
            returnFunction(socket.user.export());
    }
    disconnect(socket) {
        if (typeof socket.user === "undefined") return;
        this.disconnectedUsers.push(socket.user);
        if (socket.user.room) socket.user.room.removeUser(socket.user);
        socket.broadcast.to(socket.user.room).emit("room", {
            action: "disconnect",
            payload: { user: socket.user.export() },
        });
        socket.user.setRoom(undefined);
    }
    deleteRoom(roomId) {
        const roomIndex = this.rooms.findIndex((elm) => elm.id === roomId);
        if (roomIndex >= 0) this.rooms.splice(roomIndex, 1);
    }
}

class Room {
    constructor(id) {
        this.id = id;
        this.users = [];
        this.game = null;
        this.state = null;
        this.currentRoute = null;
    }
    addUser(user) {
        if (this.userAlreadyInRoom(user)) {
            this.removeUser(user);
        }
        this.users.push(user);
        this.giveCharacter(user);
    }
    userAlreadyInRoom(user) {
        return Boolean(this.users.find((elm) => elm.id === user.id));
    }
    giveCharacter(user) {
        const charactersRemaining = this.game.remainingCharacters.length > 0;
        const array = charactersRemaining
            ? this.game.remainingCharacters
            : this.game.characters;
        const theChoosenOne = array[Math.floor(Math.random() * array.length)];
        user.character = theChoosenOne;
        this.game.removeCharacterFromPool(theChoosenOne);
        if (!charactersRemaining) user.character.backup = true;
    }
    removeUser(user) {
        const userIndex = this.users.findIndex((elm) => elm.id === user.id);
        this.game.cleanDisconnectedUser(user);
        if (userIndex >= 0) {
            const userCharacter = this.users[userIndex].character;
            if (userCharacter && userCharacter.backup !== true)
                this.game.remainingCharacters.push(userCharacter);
            delete user.character;
            this.users.splice(userIndex, 1);
        }
    }
    setGame(gameString) {
        this.game = games[gameString]();
    }
    gameAction(data, socket, returnFunction, io) {
        let returnData;
        let entryAnswerResult;
        let finalAnswers;
        let finalChallenge;
        switch (data.action) {
            case "state":
                this.game.setState(data.payload, this, socket, io);
                break;
            case "entryAnswer":
                entryAnswerResult = this.game.manageTextAnswer(
                    "Entry",
                    data.payload.text
                );
                returnData = {
                    payload: {
                        text: data.payload.text,
                        result: entryAnswerResult,
                    },
                };
                socket.to(this).broadcast.emit("game", {
                    action: "entryAnswer",
                    ...returnData,
                });
                break;
            case "finalAnswer":
                this.game.manageTextAnswer(
                    "Final",
                    data.payload.text,
                    data.payload.id
                );
                finalAnswers = this.game.export().stepsFinalAnswers;
                returnData = {
                    payload: {
                        ...finalAnswers[data.payload.id],
                    },
                };
                socket.to(this).broadcast.emit("game", {
                    action: "finalAnswers",
                    payload: finalAnswers,
                });
                break;
            case "finalChallenge":
                this.game.manageTextAnswer(
                    "FinalChallenge",
                    data.payload.text,
                    data.payload.id
                );
                finalChallenge = this.game.export().finalChallenge;
                returnData = {
                    payload: {
                        ...finalChallenge[data.payload.id],
                    },
                };
                socket.to(this).broadcast.emit("game", {
                    action: "finalChallenge",
                    payload: finalChallenge,
                });
                break;
            case "toolPickup":
                returnData = this.game.setToolPickState(
                    socket.user,
                    data.payload
                );
                io.in(this).emit("game", {
                    action: "tools",
                    payload: this.game.export().currentMission.tools,
                });
                break;
            case "updateTool":
                returnData = this.game.updateTool(
                    data.payload.tool,
                    data.payload.data
                );
                socket.to(this).broadcast.emit("game", {
                    action: "tools",
                    payload: this.game.export().currentMission.tools,
                });
                break;
            case "revealHint":
                returnData = this.game.revealHint(data.payload.index);
                if (returnData !== false)
                    socket.to(this).broadcast.emit("game", {
                        action: "hints",
                        payload: {
                            ...returnData,
                        },
                    });
                break;
            case "setHintsCollection":
                returnData = this.game.setHints(data.payload.name);
                if (returnData !== false)
                    socket.to(this).broadcast.emit("game", {
                        action: "hints",
                        payload: {
                            ...returnData,
                        },
                    });
                break;
        }
        if (typeof returnFunction === "function") returnFunction(returnData);
    }
    export() {
        const users = this.users.map((elm) => elm.export());
        return {
            id: this.id,
            users: users,
            game: this.game.export(),
            gameName: this.game.name,
            state: this.state,
        };
    }
    toString() {
        return String(this.id);
    }
}

module.exports = RoomManager;
