const RoomManager = require("./rooms");
const roomManager = new RoomManager();

module.exports = function socketManagement(io) {
    io.on("connection", (socket) => {
        socket.on("setup", (id, returnFunction) => {
            try {
                roomManager.setupUser(id, socket, returnFunction, io);
            } catch (err) {
                console.error(err);
            }
        });
        socket.on("room", (data, returnFunction) => {
            try {
                roomManager.roomAction(data, socket, returnFunction, io);
            } catch (err) {
                console.error(err);
            }
        });
        socket.on("game", (data, returnFunction) => {
            try {
                roomManager.gameAction(data, socket, returnFunction, io);
            } catch (err) {
                console.error(err);
            }
        });
        socket.on("disconnect", () => roomManager.disconnect(socket));
    });
};
