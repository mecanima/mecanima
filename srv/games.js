const _ = require("lodash");

class Game {
    constructor(name) {
        this.characters = [];
        this.remainingCharacters = [];
        this.state = null;
        this.steps = [];
        this.tools = [];
        this.hintsMap = new Map();
        this.hints = [];
        this.hintsPoints = 0;
        this.maxHintsPoints = 0;
        this.currentMission = null;
        this.currentEntryAnswer = "";
        this.currentEntryAnswerFound = false;
        // this.currentFinalAnswer = "";
        // this.currentFinalAnswerFound = false;
        this.stepsFinalAnswers = [];
        this.name = name;
        this.pointsInterval = null;
        this.finalChallenge = [];
    }
    addCharacter(name, icon) {
        this.remainingCharacters.push({ name, icon });
    }
    addStep(entryAnswer, toolsData, finalAnswer) {
        this.steps.push({
            entryAnswer: entryAnswer.trim().toLowerCase(),
            tools: toolsData,
            finalAnswer: finalAnswer.trim().toLowerCase(),
        });
        this.stepsFinalAnswers.push({
            answer: finalAnswer.trim().toLowerCase(),
            value: "",
            found: false,
        });
    }
    addHints(collectionName, hints) {
        this.hintsMap.set(collectionName, hints);
    }
    addFinalChallenge(answer) {
        this.finalChallenge.push({
            answer: answer,
            value: "",
            found: false,
        });
    }
    removeCharacterFromPool(character) {
        const characterIndex = this.remainingCharacters.findIndex(
            (elm) => elm.name === character.name
        );
        if (characterIndex >= 0)
            this.remainingCharacters.splice(characterIndex, 1);
    }
    setStep(step) {
        this.currentMission = step;
        this.currentEntryAnswer = "";
        // this.currentFinalAnswer = "";
        this.tools = _.cloneDeep(this.steps[step].tools);
        if (this.hintsMap.has(step)) {
            this.setHints(step);
        }
    }
    setHints(step) {
        this.hints = _.cloneDeep(this.hintsMap.get(step));
        // Starts at max possible points for demo
        this.hintsPoints = this.hints.reduce((acc, curr) => acc + curr.cost, 0);
        this.maxHintsPoints = this.hints.reduce(
            (acc, curr) => acc + curr.cost,
            0
        );
        if (this.pointsInterval !== null) clearInterval(this.pointsInterval);
        const missionData = this.export().currentMission;
        return {
            ...missionData.hintsData,
        };
    }
    manageTextAnswer(answerType, text, id) {
        let result;
        if (answerType === "Entry") {
            this["currentEntryAnswer"] = text;
            const neededAnswer = this.getCurrentMission()[
                answerType.charAt(0).toLowerCase() +
                    answerType.substring(1) +
                    "Answer"
            ];
            if (neededAnswer === text.trim().toLowerCase()) {
                this["currentEntryAnswerFound"] = true;
                result = true;
            } else {
                result = false;
            }
        } else if (answerType === "Final") {
            const neededAnswerElm = this.stepsFinalAnswers[id];
            const treatedText = text.trim().toLowerCase();
            neededAnswerElm.value = text;
            if (treatedText === String(neededAnswerElm.answer)) {
                neededAnswerElm.found = true;
                result = true;
            } else {
                result = false;
            }
        } else if (answerType === "FinalChallenge") {
            const neededAnswerElm = this.finalChallenge[id];
            const treatedText = text.trim().toLowerCase();
            neededAnswerElm.value = text;
            if (treatedText === String(neededAnswerElm.answer)) {
                neededAnswerElm.found = true;
                result = true;
            } else {
                result = false;
            }
        }
        return result;
    }
    setToolPickState(user, payload) {
        const tool = this.tools.find((elm) => elm.name === payload.tool);
        if (payload.side === true) {
            if (
                typeof tool.pickedBy !== "undefined" &&
                tool.pickedBy !== null
            ) {
                return false;
            } else {
                this.tools.forEach((elm) =>
                    elm.pickedBy === user ? (elm.pickedBy = null) : null
                );
                tool.pickedBy = user;
                return true;
            }
        } else if (payload.side === false) {
            tool.pickedBy = null;
        }
        return true;
    }
    updateTool(tool, data) {
        this.tools.map((elm) => {
            if (elm.name === tool) elm.sentAnswer = data;
        });
    }
    revealHint(index) {
        const theHint = this.hints[index];
        if (theHint.cost <= this.hintsPoints) {
            theHint.hidden = false;
            this.hintsPoints -= theHint.cost;
            const missionData = this.export().currentMission;
            return {
                ...missionData.hintsData,
            };
        } else {
            return false;
        }
    }
    cleanDisconnectedUser(user) {
        this.tools.forEach((elm) => {
            if (elm.pickedBy && elm.pickedBy.id === user.id) {
                elm.pickedBy = null;
            }
        });
    }
    getCurrentMission() {
        return this.steps[this.currentMission];
    }
    setState(payload, room, socket, io) {
        this.state = payload.state;
        if (typeof payload.mission === "number") {
            this.setStep(payload.mission);
            // Disable auto increasing of points count
            // this.pointsInterval = setInterval(
            //     () => {
            //         this.hintsPoints++;
            //         io.in(room).emit("game", {
            //             action: "hintsPoint",
            //             payload: { points: this.hintsPoints },
            //         });
            //         if (this.hintsPoints >= this.maxHintsPoints)
            //             clearInterval(this.pointsInterval);
            //     },
            //     process.env.NODE_ENV === "production" ? 60 * 1000 : 10 * 1000
            // );

            if (this.hintsMap.has(this.state)) {
                this.setHints(this.state);
            }
            io.in(room).emit("game", {
                action: "step",
                payload: { game: this.export() },
            });
        } else {
            if (this.hintsMap.has(this.state)) {
                this.setHints(this.state);
            }
            io.to(room).emit("game", {
                action: "state",
                payload: {
                    state: payload.state,
                    hintsData: this.export().currentMission.hintsData,
                },
            });
        }
    }
    export() {
        if (this.currentMission !== null) {
            const currentStepReference = this.getCurrentMission();
            const entryAnswer = {
                text: this.currentEntryAnswer,
                length: currentStepReference.entryAnswer.length,
                result: this.currentEntryAnswerFound,
            };
            const finalAnswer = {
                text: this.currentFinalAnswer,
                length: currentStepReference.finalAnswer.length,
                result: this.currentFinalAnswerFound,
            };
            const tools = this.tools.map((elm) => ({
                name: elm.name,
                sentAnswer: elm.sentAnswer,
                pickedBy: elm.pickedBy ? elm.pickedBy.export() : null,
            }));
            const hints = this.hints.map((elm) =>
                elm.hidden === true ? { cost: elm.cost } : elm
            );

            const stepsFinalAnswers = this.stepsFinalAnswers.map((elm) => ({
                value: elm.value,
                found: elm.found,
            }));

            const finalChallenge = this.finalChallenge.map((elm) => ({
                value: elm.value,
                found: elm.found,
            }));
            return {
                state: this.state,
                name: this.name,
                currentMission: {
                    entryAnswer,
                    finalAnswer,
                    tools,
                    hintsData: {
                        maxHintsPoints: this.maxHintsPoints,
                        hintsPoints: this.hintsPoints,
                        hints,
                    },
                },
                finalChallenge,
                stepsFinalAnswers,
            };
        } else {
            return { state: this.state, name: this.name };
        }
    }
}

function instrumentsScientifiquesFactory() {
    const InstrumentsScientifiques = new Game("is");
    InstrumentsScientifiques.addCharacter("Wilhelm Röntgen", "wilhelm");
    InstrumentsScientifiques.addCharacter("Galilée", "galilee");
    InstrumentsScientifiques.addCharacter("Ferdinand Berthoud", "ferdinand");
    InstrumentsScientifiques.addCharacter("Eugène Bourdon", "eugene");
    InstrumentsScientifiques.addHints("mission1-intro", [
        {
            text: "C'est un instrument clé de l'astronomie du 17ème siècle",
            cost: 1,
            hidden: true,
        },
        {
            text:
                "Le scientifique qui a affirmé que la Terre tournait autour du Soleil venait d’Italie",
            cost: 2,
            hidden: true,
        },
        {
            text:
                "Florence ne fut pas qu’un haut lieu artistique pour Michel-Ange",
            cost: 3,
            hidden: true,
        },
    ]);
    InstrumentsScientifiques.addHints("tools-menu", [
        {
            text: "Avez-vous bien lu la notice de la lunette astronomique ?",
            cost: 1,
            hidden: true,
        },
        {
            text:
                "Le maître du temps et de l’orientation doivent lier leurs connaissances pour repérer la bonne Lune dans le ciel",
            cost: 2,
            hidden: true,
        },
        {
            text:
                "Le premier quartier est le plus propice à l’observation. Votre réponse se cache dans les cratères de la Lune",
            cost: 3,
            hidden: true,
        },
    ]);
    InstrumentsScientifiques.addHints("end", [
        {
            text:
                "Vous avez vu la pascaline dans la collection des instruments scientifiques ?",
            cost: 1,
            hidden: true,
        },
        {
            text:
                "Cette maquette se trouve non loin des bâtons de Neper qui permettent les multiplications dans une petite pièce à l’ouest des violons",
            cost: 2,
            hidden: true,
        },
        {
            text: "La maquette se trouve dans l’Atelier",
            cost: 3,
            hidden: true,
        },
    ]);
    InstrumentsScientifiques.addStep(
        "Florence",
        [
            {
                name: "compass",
                answer: 140,
                sentAnswer: 0,
            },
            {
                name: "calendar",
                answer: 0,
                sentAnswer: 5,
            },
            {
                name: "lenses",
                answer: 0,
                sentAnswer: ["convergent", "convergent"],
            },
            {
                name: "telescope",
            },
        ],
        "17"
    );
    InstrumentsScientifiques.addFinalChallenge("9999");
    InstrumentsScientifiques.addFinalChallenge("1794");
    return InstrumentsScientifiques;
}

module.exports = {
    instrumentsScientifiques: instrumentsScientifiquesFactory,
};
